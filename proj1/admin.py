from django import forms
from django.contrib import admin
from django.contrib.auth.models import User

from track.models import (VisceralFatRating, BoneMass, BloodPressure, BodyMassIndex,
                          BodyWater, BodyWeight, LeanBodyMass, BodyFat, Sleep, Steps,
                          BloodGlucose, Fitness, SyncLog, BloodGlucoseAccount, HealthTrait,
                          HealthDrugResponse, HealthRisk, HealthCarrier)


class VisceralFatRatingAdminForm(forms.ModelForm):

    class Meta:
        model = VisceralFatRating

    def __init__(self, *args, **kwds):
        super(VisceralFatRatingAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class VisceralFatRatingAdmin(admin.ModelAdmin):

    """
    Admin for VisceralFat
    """
    model = VisceralFatRating
    list_display = ('user', 'rating', 'date_checked')
    form = VisceralFatRatingAdminForm


class BoneMassAdminForm(forms.ModelForm):

    class Meta:
        model = BoneMass

    def __init__(self, *args, **kwds):
        super(BoneMassAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class BloodGlucoseAdmin(admin.ModelAdmin):

    """
    Admin for BloodGlucose
    """
    model = BloodGlucose
    list_display = ('user', 'glucose', 'insulin', 'date_checked')


class BloodGlucoseAccountAdmin(admin.ModelAdmin):

    """
    Admin for BloodGlucoseAccount
    """
    model = BloodGlucoseAccount
    list_display = ('user', 'email')


class BoneMassAdmin(admin.ModelAdmin):

    """
    Admin for BoneMass
    """
    model = BoneMass
    list_display = ('user', 'bone_mass', 'date_checked')

    form = BoneMassAdminForm


class BloodPressureAdminForm(forms.ModelForm):

    class Meta:
        model = BloodPressure

    def __init__(self, *args, **kwds):
        super(BloodPressureAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class BloodPressureAdmin(admin.ModelAdmin):

    """
    Admin for BloodPressure
    """
    model = BloodPressure
    list_display = ('user', 'systolic', 'diastolic', 'date_checked')

    form = BloodPressureAdminForm


class BodyMassIndexAdminForm(forms.ModelForm):

    class Meta:
        model = BodyMassIndex

    def __init__(self, *args, **kwds):
        super(BodyMassIndexAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class BodyMassIndexAdmin(admin.ModelAdmin):

    """
    Admin for Body Mass Index
    """
    model = BodyMassIndex
    list_display = ('bmi', 'height_ft', 'height_inch', 'weight', 'date_checked')
    readonly_fields = ('bmi',)

    form = BodyMassIndexAdminForm


class BodyWaterAdminForm(forms.ModelForm):

    class Meta:
        model = BodyWater

    def __init__(self, *args, **kwds):
        super(BodyWaterAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class BodyWaterAdmin(admin.ModelAdmin):

    """
    Admin for Body Water
    """
    model = BodyWater
    list_display = ('user', 'body_water', 'date_checked')

    form = BodyWaterAdminForm


class BodyWeightAdminForm(forms.ModelForm):

    class Meta:
        model = BodyWeight

    def __init__(self, *args, **kwds):
        super(BodyWeightAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class BodyWeightAdmin(admin.ModelAdmin):

    """
    Admin for Body Water
    """
    model = BodyWeight
    list_display = ('user', 'weight', 'date', 'time')

    form = BodyWeightAdminForm


class LeanBodyMassAdminForm(forms.ModelForm):

    class Meta:
        model = LeanBodyMass

    def __init__(self, *args, **kwds):
        super(LeanBodyMassAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class LeanBodyMassAdmin(admin.ModelAdmin):

    """
    Admin for Lean Body Mass
    """
    model = LeanBodyMass
    list_display = ('user', 'lean_body_mass', 'date_checked')

    form = LeanBodyMassAdminForm


class BodyFatAdminForm(forms.ModelForm):

    class Meta:
        model = BodyFat

    def __init__(self, *args, **kwds):
        super(BodyFatAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class BodyFatAdmin(admin.ModelAdmin):

    """
    Admin for Body Body Fat
    """
    model = BodyFat
    list_display = ('user', 'body_fat', 'date_checked')

    form = BodyFatAdminForm


class SleepAdminForm(forms.ModelForm):

    class Meta:
        model = Sleep

    def __init__(self, *args, **kwds):
        super(SleepAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class SleepAdmin(admin.ModelAdmin):
    model = Sleep
    list_display = ('user', 'start', 'stop', 'hours')

    form = SleepAdminForm


class StepsAdminForm(forms.ModelForm):

    class Meta:
        model = Steps

    def __init__(self, *args, **kwds):
        super(StepsAdminForm, self).__init__(*args, **kwds)

        filter = User.objects.order_by('first_name', 'last_name')
        self.fields['user'].queryset = filter


class StepsAdmin(admin.ModelAdmin):

    """
    Admin for Body Water
    """
    model = Steps
    list_display = ('user', 'steps', 'date', 'time')
    form = StepsAdminForm


class FitnessAdmin(admin.ModelAdmin):

    """
    Admin for Fitness
    """
    model = Fitness

    list_display = (
        'user',
        'timestamp',
        'activity_type',
        'distance',
        'calories',
        'duration'
    )


class SyncLogAdmin(admin.ModelAdmin):

    """
    Admin for Sync Log
    """
    model = SyncLog


class HealthRiskAdmin(admin.ModelAdmin):

    """
    Admin for HealthRisk
    """
    model = HealthRisk


class HealthDrugResponseAdmin(admin.ModelAdmin):

    """
    Admin for HealthDrugResponse
    """
    model = HealthDrugResponse


class HealthTraitAdmin(admin.ModelAdmin):

    """
    Admin for HealthTrait
    """
    model = HealthTrait


class HealthCarrierAdmin(admin.ModelAdmin):

    """
    Admin for HealthCarrier
    """
    model = HealthCarrier

admin.site.register(Sleep, SleepAdmin)
admin.site.register(VisceralFatRating, VisceralFatRatingAdmin)
admin.site.register(BoneMass, BoneMassAdmin)
admin.site.register(BloodPressure, BloodPressureAdmin)
admin.site.register(BodyMassIndex, BodyMassIndexAdmin)
admin.site.register(BodyWater, BodyWaterAdmin)
admin.site.register(BodyWeight, BodyWeightAdmin)
admin.site.register(BodyFat, BodyFatAdmin)
admin.site.register(LeanBodyMass, LeanBodyMassAdmin)
admin.site.register(Steps, StepsAdmin)
admin.site.register(BloodGlucose, BloodGlucoseAdmin)
admin.site.register(BloodGlucoseAccount, BloodGlucoseAccountAdmin)
admin.site.register(Fitness, FitnessAdmin)
admin.site.register(SyncLog, SyncLogAdmin)
admin.site.register(HealthCarrier, HealthCarrierAdmin)
admin.site.register(HealthDrugResponse, HealthDrugResponseAdmin)
admin.site.register(HealthRisk, HealthRiskAdmin)
admin.site.register(HealthTrait, HealthTraitAdmin)
