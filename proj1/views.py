# Python
import datetime
import json
import time
import logging
import dateutil
import calendar
import subprocess


from collections import OrderedDict

# Django
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.exceptions import MultipleObjectsReturned
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers.json import DjangoJSONEncoder
from django.forms.formsets import formset_factory, BaseFormSet
from django.core.context_processors import csrf
from django.utils.functional import curry

from paypal.standard.forms import PayPalPaymentsForm

# Project
from accounts.models import GeneticProfile, UserProfile, PhysicianSubscriptions, Plan
from billing import CreditCard, get_gateway
from billing.forms import CreditCardForm
from devices import constants
from devices.clients import FitBitOAuthClient, api_client_factory
from devices.models import DeviceAccount, Device
from foods.models import Food, FoodMetricGroup, FoodMetricList, FoodMetric, FoodIntake
from nutritas.utils import user_average_bp, PerSessionThreading, group_required
from products.models import Product, PUBLISHED
from telemedicine.models import Session
from telemedicine.subscription import models as telmed_sub_mods
from track import client as ttam_client
from track.forms import SleepForm, AddPatientForm
from track.models import (VisceralFatRating, BloodPressure, BodyMassIndex, BodyWater,
                          BodyWeight, BoneMass, LeanBodyMass, BodyFat, Sleep, Steps,
                          Floor, Calorie, MuscleMass, BodyWeightScale, CaloriesScale,
                          SystolicBloodPressureScale, DiastolicBloodPressureScale,
                          BloodGlucoseScale, SleepScale, StepScale, BloodGlucose,
                          CalorieIntakeScale, Fitness)
from track.utils import (blood_pressure_chart_data, generate_series_data,
                         update_scale_min_max, calculate_scale_default,
                         check_if_within_threshold, get_current_sleep_average,
                         get_current_step_average, get_current_weight,
                         get_current_systolic_bp, get_current_diastolic_bp,
                         get_current_blood_glucose, get_current_calories_burned,
                         get_current_calorie_intake_average, pull_data_for_validic,
                         blood_glucose_chart_data, blood_glucose_fasting_glucose_chart_data,
                         blood_glucose_oral_glucose_chart_data, blood_glucose_random_glucose_chart_data,
                         get_calories, get_total_calories, get_week_startdate, render_chart_data)
from track.validic import Validic


logger = logging.getLogger(__name__)


def index(request, template='track/index.html'):
    """
    index page for track

    **Context**
    ``RequestContext``

    ``products``
    :model:`products.Product`

    **Template:**
    :template:`track/index.html`

    """
    context = {}
    context["devices"] = Product.objects.filter(title__category__title="Devices", status=PUBLISHED)
    return render_to_response(template, context, RequestContext(request))


@login_required
def profile(request, patient_id=None, template='track/profile.html'):
    """
    user profile page for track

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    ``accounts``
    :model:`accounts.PhysicianSubscriptions`

    **Template:**
    :template:`track/profile.html`
    """
    from track.utils import (
        get_ttam_risks,
        get_ttam_carriers,
        get_ttam_drug_responses,
        get_ttam_traits
    )

    context = {}

    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        active_plan = None
        try:
            active_plan = telmed_sub_mods.PatientPlan.objects.get(user=patient, active=True)
        except telmed_sub_mods.PatientPlan.MultipleOjbectsReturned, e:
            active_plan = telmed_sub_mods.PatientPlan.objects.filter(user=patient, active=True).order_by('-date_created')[0]
        except telmed_sub_mods.PatientPlan.DoesNotExist:
            resp = render_to_response('403.html', context_instance=RequestContext(request))
            resp.status_code = 403
            return resp

        if active_plan and active_plan.active_coach and active_plan.active_coach == request.user:
            pass
        else:
            resp = render_to_response('403.html', context_instance=RequestContext(request))
            resp.status_code = 403
            return resp

        device_user = patient
        check_if_within_threshold(device_user, request.user)
    else:
        device_user = request.user

    context['patient'] = patient
    context['risks'] = get_ttam_risks(device_user)
    context['carriers'] = get_ttam_carriers(device_user)
    context['drug_responses'] = get_ttam_drug_responses(device_user)
    context['traits'] = get_ttam_traits(device_user)

    # pull_data_for_validic(device_user)

    # uncomment after
    # pull_data_for_validic(device_user)
    # Load fitbit data
    # if DeviceAccount.objects.filter(userprofile=device_user.userprofile, provider=constants.FITBIT_PROVIDER).exists():
    #     account = DeviceAccount.objects.get(userprofile=device_user.userprofile, provider=constants.FITBIT_PROVIDER)
    #     client = api_client_factory(account)
    #     method = client.fetch_all

    #     thread = PerSessionThreading(request.session, target=method)
    #     thread.start()

    return render_to_response(template, context, RequestContext(request))


@login_required
def visceral_fat(request, patient_id=None, template='track/profile.html'):
    """
    shows users visceral fat rating

    **Context**
    ``RequestContext``

    **Template:**
    :template:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(
            request,
            "visceral_fat",
            "date_checked",
            "daily",
            "average",
            patient
        )
    else:
        chart_data = get_activity_chart_data(
            request,
            "visceral_fat",
            "date_checked",
            "daily",
            "average"
        )

    if request.is_ajax():
        data = json.dumps(chart_data)
        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = chart_data
        return render_to_response(template, context, RequestContext(request))


@login_required
def blood_pressure(request, patient_id=None, template='track/profile.html'):
    """
    shows users blood pressure rating

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    ``track``
    :model:`track.BloodPressure`

    **Template:**
    :template:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    # TODO : refine later after instructions are given
    if patient:
        ratings = BloodPressure.objects.filter(user=patient)
    else:
        ratings = BloodPressure.objects.filter(user=request.user)

    date_string = ""
    diastolic_string = ""
    systolic_string = ""

    months = []
    today = datetime.date.today()
    for i in range(1, 13):
        month_now = datetime.date(today.year, i, 1)
        months.append(month_now.strftime('%B'))

        bp = user_average_bp(request.user, month_now)
        if i < 12:
            systolic_string += str(bp[0]) + ", "
            diastolic_string += str(bp[1]) + ", "
        else:
            systolic_string += str(bp[0])
            diastolic_string += str(bp[1])

    for index, month in enumerate(months):
        if index < 11:
            date_string += "'" + month + "' ,"
        else:
            date_string += "'" + month + "'"

    context = {}

    context["blood_pressure"] = True

    context['chart_data'] = blood_pressure_chart_data(date_string, systolic_string, diastolic_string)

    return render_to_response(template, context, RequestContext(request))


@login_required
def blood_glucose_ajax(request, patient_id=None, mimetype="application/json"):
    """
    shows user blood glucose data
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if request.is_ajax():
        context = {}
        glucoses = []
        insulins = []
        series = []
        categories = []

        date_filter = request.GET['date_range'] if 'date_range' in request.GET else "hourly"
        date_range = request.GET.get("date_range", "hourly")

        if patient:
            insulins = BloodGlucose.objects.filter(user=request.user).date_filter('date_checked', date_range).order_by('date_checked')
        else:
            insulins = BloodGlucose.objects.filter(user=patient).date_filter('date_checked', date_range).order_by('date_checked')

        insulin = generate_series_data(insulins, 'date_checked', date_range, 'insulin', 'average', True)
        insulin_series = {'name': 'C Peptide', 'data': insulin}
        series.append(insulin_series)

        categories = []
        for data in glucoses:
            categories.append(data[0])

        chart_data = blood_glucose_chart_data(categories, insulin_series)

        data = json.dumps(chart_data)
        return HttpResponse(data)


@login_required
def blood_glucose_c_peptide_ajax(request, patient_id=None, mimetype="application/json"):
    """
    shows the user blood glucose c peptide

    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if request.is_ajax():
        context = {}
        glucoses = []
        insulins = []
        series = []
        categories = []

        date_filter = request.GET['date_range'] if 'date_range' in request.GET else "hourly"
        date_range = request.GET.get("date_range", "hourly")

        if patient:
            c_peptides = BloodGlucose.objects.filter(user=patient).date_filter('date_checked', date_range).order_by('date_checked')
        else:
            c_peptides = BloodGlucose.objects.filter(user=request.user).date_filter('date_checked', date_range).order_by('date_checked')

        c_peptide = generate_series_data(c_peptides, 'date_checked', date_range, 'c_peptide', 'average', True)
        c_peptide_series = {'name': 'C Peptide', 'data': c_peptide}
        series.append(c_peptide_series)

        categories = []
        for data in glucoses:
            categories.append(data[0])

        chart_data = blood_glucose_c_peptide_chart_data(categories, c_peptide_series)

        data = json.dumps(chart_data)
        return HttpResponse(data)


@login_required
def blood_glucose_fasting_glucose_ajax(request, patient_id=None, mimetype="application/json"):
    """
    shows the user fasting glucose
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if request.is_ajax():
        context = {}
        glucoses = []
        insulins = []
        series = []
        categories = []

        date_filter = request.GET['date_range'] if 'date_range' in request.GET else "hourly"
        date_range = request.GET.get("date_range", "hourly")

        if patient:
            fasting_plasmas = BloodGlucose.objects.filter(user=patient).date_filter('date_checked', date_range).order_by('date_checked')
        else:
            fasting_plasmas = BloodGlucose.objects.filter(user=request.user).date_filter('date_checked', date_range).order_by('date_checked')

        fasting_plasma = generate_series_data(fasting_plasmas, 'date_checked', date_range, 'fasting_plasma_glucose_test', 'average', True)
        fasting_plasma_series = {'name': 'Fasting Plasma', 'data': fasting_plasma}
        series.append(fasting_plasma_series)

        categories = []
        for data in glucoses:
            categories.append(data[0])

        chart_data = blood_glucose_fasting_glucose_chart_data(categories, fasting_plasma_series)

        data = json.dumps(chart_data)
        return HttpResponse(data)


@login_required
def blood_glucose_oral_glucose_ajax(request, patient_id=None, mimetype="application/json"):
    """
    shows the user oral glucose

    ***Context***
    ``RequestContext``

    ``user``
    :model:`auth.User`

    ``track``
    :model:`track.BloodGlucose`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if request.is_ajax():
        context = {}
        glucoses = []
        insulins = []
        series = []
        categories = []

        date_filter = request.GET['date_range'] if 'date_range' in request.GET else "hourly"
        date_range = request.GET.get("date_range", "hourly")

        if patient:
            oral_glucoses = BloodGlucose.objects.filter(user=patient).date_filter('date_checked', date_range).order_by('date_checked')
        else:
            oral_glucoses = BloodGlucose.objects.filter(user=request.user).date_filter('date_checked', date_range).order_by('date_checked')

        oral_glucose = generate_series_data(oral_glucoses, 'date_checked', date_range, 'oral_glucose_tolerance_test', 'average', True)
        oral_glucose_series = {'name': 'Oral Glucose Tolerance', 'data': oral_glucose}
        series.append(oral_glucose_series)

        categories = []
        for data in glucoses:
            categories.append(data[0])

        chart_data = blood_glucose_oral_glucose_chart_data(categories, oral_glucose_series)

        data = json.dumps(chart_data)
        return HttpResponse(data)


@login_required
def blood_glucose_random_glucose_ajax(request, patient_id=None, mimetype="application/json"):
    """
    shows the user blood glucose

    ***Context***
    ``RequestContext``

    ``user``
    :model:`auth.User`

    ``track``
    :model:`track.BloodGlucose`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if request.is_ajax():
        context = {}
        glucoses = []
        insulins = []
        series = []
        categories = []

        date_filter = request.GET['date_range'] if 'date_range' in request.GET else "hourly"
        date_range = request.GET.get("date_range", "hourly")

        if patient:
            random_glucoses = BloodGlucose.objects.filter(user=patient).date_filter('date_checked', date_range).order_by('date_checked')
        else:
            random_glucoses = BloodGlucose.objects.filter(user=request.user).date_filter('date_checked', date_range).order_by('date_checked')

        random_glucose = generate_series_data(random_glucoses, 'date_checked', date_range, 'random_plasma_glucose_test', 'average', True)
        random_glucose_series = {'name': 'Oral Glucose Tolerance', 'data': random_glucose}
        series.append(random_glucose_series)

        categories = []
        for data in glucoses:
            categories.append(data[0])

        chart_data = blood_glucose_random_glucose_chart_data(categories, random_glucose_series)

        data = json.dumps(chart_data)
        return HttpResponse(data)


@login_required
def blood_glucose_triglyceride_ajax(request, patient_id=None, mimetype="application/json"):
    """
    shows the user triglyceride
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if request.is_ajax():
        context = {}
        glucoses = []
        insulins = []
        series = []
        categories = []

        date_filter = request.GET['date_range'] if 'date_range' in request.GET else "hourly"
        date_range = request.GET.get("date_range", "hourly")

        if patient:
            triglycerides = BloodGlucose.objects.filter(user=patient).date_filter('date_checked', date_range).order_by('date_checked')
        else:
            triglycerides = BloodGlucose.objects.filter(user=request.user).date_filter('date_checked', date_range).order_by('date_checked')

        triglyceride = generate_series_data(triglycerides, 'date_checked', date_range, 'triglyceride', 'average', True)
        triglyceride_series = {'name': 'Triglyceride', 'data': triglyceride}
        series.append(triglyceride_series)

        categories = []
        for data in glucoses:
            categories.append(data[0])

        chart_data = blood_glucose_triglyceride_chart_data(categories, triglyceride_series)

        data = json.dumps(chart_data)
        return HttpResponse(data)


@login_required
def blood_pressure_ajax(request, patient_id=None, mimetype="application/json"):
    """
    shows the user blood pressure rating
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if request.is_ajax():
        context = {}
        systolics = []
        diastolics = []
        series = []

        # months = []
        # today = datetime.date.today()

        # for i in range(1, 13):
        #     month_now = datetime.date(today.year, i, 1)
        #     months.append(month_now.strftime('%B'))

        #     bp = user_average_bp(request.user, month_now)

        #     systolics.append(bp[0])
        #     diastolics.append(bp[1])

        # context = {}
        # series = []
        # systolic_series = {'name': 'Systolic', 'data': systolics}
        # diastolic_series = {'name': 'Diastolic', 'data': diastolics}
        # series.append(systolic_series)
        # series.append(diastolic_series)

        # context["categories"] = None #months
        # context["series"] = series
        date_filter = request.GET['date_range'] if 'date_range' in request.GET else datetime.time()
        date_range = request.GET.get("date_range", "daily")

        if patient:
            blood_pressures = BloodPressure.objects.filter(user=patient).date_filter('date_checked', date_range).order_by('date_checked')
        else:
            blood_pressures = BloodPressure.objects.filter(user=request.user).date_filter('date_checked', date_range).order_by('date_checked')

        systolics = generate_series_data(blood_pressures, 'date_checked', date_range, 'systolic', 'average', True)
        diastolics = generate_series_data(blood_pressures, 'date_checked', date_range, 'diastolic', 'average', True)
        systolic_series = {'name': 'Systolic', 'data': systolics}
        diastolic_series = {'name': 'Diastolic', 'data': diastolics}
        series.append(systolic_series)
        series.append(diastolic_series)

        categories = []
        for data in systolics:
            categories.append(data[0])

        chart_data = blood_pressure_chart_data(categories, systolic_series, diastolic_series)

        data = json.dumps(chart_data)

        return HttpResponse(data)


@login_required
def body_mass_index(request, patient_id=None, template='track/profile.html'):
    """
    shows the user's body mass index

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    **Template:**
    :template:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(
            request,
            "body_mass_index",
            "date_checked",
            "daily",
            "average",
            patient
        )
    else:
        chart_data = get_activity_chart_data(
            request,
            "body_mass_index",
            "date_checked",
            "daily",
            "average"
        )

    if request.is_ajax():
        data = json.dumps(chart_data)
        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = chart_data
        return render_to_response(template, context, RequestContext(request))


@login_required
def body_water(request, patient_id=None, template='track/profile.html'):
    """
    shows user's body water percentage

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    **Template:**

    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(
            request,
            "body_water",
            "date_checked",
            "daily",
            "average",
            patient
        )
    else:
        chart_data = get_activity_chart_data(
            request,
            "body_water",
            "date_checked",
            "daily",
            "average",
            patient
        )

    if request.is_ajax():
        data = json.dumps(chart_data)
        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = chart_data
        return render_to_response(template, context, RequestContext(request))


@login_required
def body_weight(request, patient_id=None, template='track/profile.html'):
    """
    shows user's body weight

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    ``track``
    :model:`track.BodyWeight`

    **Template**
    :template:`track/profile.html`

    TODO:
    Goal Weight
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    date_filter = request.GET.get('date_range', 'hourly')
    if patient:
        body_weights = BodyWeight.objects.filter(user=patient).date_filter('date', date_filter)
    else:
        body_weights = BodyWeight.objects.filter(user=request.user).date_filter('date', date_filter)
    series_data = generate_series_data(body_weights, 'date', date_filter, 'weight', 'average', formatted=True)
    # Dummy goal weight
    goal_weight = 2

    # Dictionary for main chart items
    chart_data = {
        "chart": {"type": "column"},
        "colors": [
            "#2F7ED8",
            "#00FF00",
            "#FF0000",
        ],
        "legend": {
            "layout": "vertical",
            "align": "right",
            "verticalAlign": "middle",
            "borderWidth": 0
        },
        "series": [
            {
                "name": "Weight",
                "data": series_data,
            }
            # , {
            #     "name": "Goal %.02f lbs" % goal_weight,
            #     "data": [{
            #             "x": 0,
            #             "y": goal_weight
            #         },{
            #             "x": len(series_data) - (1 if len(series_data) else 0),
            #             "y": goal_weight
            #         }
            #     ]
            # }
        ],
        "title": {
            "text": "Pounds (lbs)"
        },
        "xAxis": {
            "categories": [data[0] for data in series_data],
        },
        "yAxis": {
            "title": {
                "text": "Pounds (lbs)"
            },
            "plotLines": [{
                "value": 0,
                "width": 1,
                "color": "#808080"
            }]
        }
    }

    """
    Append to series only if there are two or more
    Body weight entries to show trend.
    Modify chart title to indicate trend average
    """
    if series_data and len(series_data) > 1:
        last_weight_index = len(series_data) - 1

        chart_data["series"].append({
            "name": "Weight Trend",
            "data": [{
                "x": 0,
                "y": [sd for sd in series_data][0] + [sd for sd in series_data][last_weight_index]
            }]
        })

        # Data for title trend average
        first = [sd[1] for sd in series_data][0]
        last = [sd[1] for sd in series_data][last_weight_index]
        difference = last - first

        # Modify chart title
        chart_data["title"]["text"] = "Trend: %.02f call - %s" % ((difference / len(series_data)), date_filter)

    if request.is_ajax():
        data = json.dumps(chart_data)

        return HttpResponse(data)
    else:
        context = {}
        context['chart_data'] = str(chart_data)
        return render_to_response(template, context, RequestContext(request))


@login_required
def bone_mass(request, patient_id=None, template='track/profile.html'):
    """
    shows user's bone mass

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    **Template:**
    :template:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(
            request,
            "bone_mass",
            "date_checked",
            "daily",
            "average",
            patient
        )
    else:
        chart_data = get_activity_chart_data(
            request,
            "bone_mass",
            "date_checked",
            "daily",
            "average"
        )

    if request.is_ajax():
        data = json.dumps(chart_data)
        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = chart_data
        return render_to_response(template, context, RequestContext(request))


@login_required
def sleep(request, template='track/sleep.html'):
    """
    user page for sleep data

    **Context**
    ``RequestContext``

    ``track``
    :model:`track.Sleep`

    **Template:**
    :template:`track/sleep.html`
    """
    context = {}
    context['sleep'] = Sleep.objects.filter(user=request.user)
    context['sleep_form'] = SleepForm()
    return render_to_response(template, context, RequestContext(request))


@login_required
def lean_body_mass(request, patient_id=None, template='track/profile.html'):
    """
    show user's lean body mass

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    **Template:**
    :template:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(
            request,
            "lean_body_mass",
            "date_checked",
            "daily",
            "average",
            patient
        )
    else:
        chart_data = get_activity_chart_data(
            request,
            "lean_body_mass",
            "date_checked",
            "daily",
            "average"
        )

    if request.is_ajax():
        data = json.dumps(chart_data)
        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = chart_data
        return render_to_response(template, context, RequestContext(request))


@login_required
def body_fat(request, patient_id=None, template='track/profile.html'):
    """
    show user's body fat

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    **Template:**
    :template:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(
            request,
            "body_fat",
            "date_checked",
            "daily",
            "average",
            patient
        )
    else:
        chart_data = get_activity_chart_data(
            request,
            "body_fat",
            "date_checked",
            "daily",
            "average"
        )

    if request.is_ajax():
        data = json.dumps(chart_data)
        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = chart_data
        return render_to_response(template, context, RequestContext(request))


@login_required
def muscle_mass(request, patient_id=None, template='track/profile.html'):
    """
    show user's body fat

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    **Template:**
    :template:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(
            request,
            "muscle_mass",
            "date_checked",
            "daily",
            "average",
            patient
        )
    else:
        chart_data = get_activity_chart_data(
            request,
            "muscle_mass",
            "date_checked",
            "daily",
            "average"
        )

    if request.is_ajax():
        data = json.dumps(chart_data)
        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = chart_data
        return render_to_response(template, context, RequestContext(request))


def ajax_sleep(request, patient_id=None):
    """ Get high chart properties and page display values """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if not request.is_ajax():
        return HttpResponseNotFound()

    date_range = request.GET.get('date_range', 'daily')
    if patient:
        sleep = Sleep.objects.filter(user=patient).date_filter('timestamp', date_range).order_by('timestamp')
    else:
        sleep = Sleep.objects.filter(user=request.user).date_filter('timestamp', date_range).order_by('timestamp')
    context = {}

    if not sleep:
        return HttpResponse()

    context['xaxis'] = None
    context['yaxis_title'] = None
    context['yaxis_type'] = None

    # high chart properties
    if request.GET.get('graph') == '3':
        context['chart_type'] = 'pie'
        light, deep, awake = 0, 0, 0
        for sleep_info in sleep:
            light += sleep_info.light_hours
            #rem += sleep_info.rem_hours
            deep += sleep_info.deep_hours
            awake += sleep_info.awake_hours

        light = round(light / sleep.count(), 2)
        #rem = round(rem/sleep.count(), 2)
        deep = round(deep / sleep.count(), 2)
        awake = round(awake / sleep.count(), 2)

        total_sleep = float(light + deep + awake)
        actual_awake = 0
        if total_sleep:
            actual_awake = round(24 - total_sleep, 2)

        #awake = round((datetime.timedelta(24)-ave_hrs).seconds/3600.0, 2)
        #asleep = round(ave_hrs.seconds/3600.0, 2)

        context['series'] = [{
            "name": "hours",
            "data": [["awakened", awake], ["light", light], ["deep", deep], ["awake", actual_awake]]
        }]

    else:
        context['chart_type'] = 'column'

        #series_data = generate_series_data(sleep, 'stop', date_range, 'hours', annotation_type="average", formatted=True)
        light_series = generate_series_data(sleep, 'timestamp', date_range, 'light_hours', annotation_type="average", formatted=True)
        deep_series = generate_series_data(sleep, 'timestamp', date_range, 'deep_hours', annotation_type="average", formatted=True)
        #rem_series = generate_series_data(sleep, 'timestamp', date_range, 'rem_hours', annotation_type="average", formatted=True)
        awake_series = generate_series_data(sleep, 'timestamp', date_range, 'awake_hours', annotation_type="average", formatted=True)
        context['xaxis'] = [data[0] for data in light_series]
        context['yaxis_title'] = 'Hours Slept'
        context['yaxis_type'] = 'linear'
        context['series'] = [
            #{"name": "total hours",  "data": series_data },
            {"name": "awake", "data": awake_series},
            {"name": "light sleep", "data": light_series},
            #{"name": "rem",  "data": rem_series},
            {"name": "deep sleep", "data": deep_series}
        ]

    return HttpResponse(simplejson.dumps(context), mimetype='text/json')


@login_required
def ajax_sleep_submit(request):
    """
    Saves manual input of sleep data via ajax
    """
    if not request.is_ajax():
        return HttpResponseNotFound()

    format = request.GET.copy()
    format["start"] = parser.parse(request.GET["start"])
    format["stop"] = parser.parse(request.GET["stop"])
    form = SleepForm(format)

    if form.is_valid():
        sleep = form.save(commit=False)
        sleep.user = request.user
        sleep.awaken = 0  # TODO: temporary values
        sleep.fell_asleep_in = 0  # TODO: temporary values
        sleep.save()

    return HttpResponse()


@login_required
def track_settings(request, template='track/settings.html'):
    """
    Displays devices

    **Context**
    ``RequestContext``

    **Template:**
    :template:`track/settings.html`
    """
    # try:
    #     user_profile = UserProfile.objects.get(user=request.user)
    # except UserProfile.DoesNotExist as e:
    #     logger.error(e)
    #     return HttpResponseNotFound()

    # context = {}
    # context['my_devices'] = user_profile.devices.all()
    # device_ids = context['my_devices'].values_list('pk', flat=True)
    # context['other_devices'] = Device.objects.exclude(pk__in=device_ids)
    context = {}
    synced_apps = []
    unsynced_apps = []
    owner = request.user
    current_protocol = "http://"

    if request.is_secure():
        current_protocol = "https://"

    # Validic Devices
    validic = Validic(
        settings.SB_ORGANIZATION_APP_ID,
        settings.SB_ORGANIZATION_SECRET,
        settings.SB_ORGANIZATION_ACCESS_TOKEN
    )
    devices = settings.DEVICES

    validic_profile = validic.get_validic_profile(owner)
    apps = validic.get_apps(validic_profile, devices)

    current_site = current_protocol + Site.objects.get_current().domain

    redirect_url = current_site + '/track/profile/devices'
    redirect_param = '&redirect_uri=' + redirect_url

    for device in apps['synced']:
        app = {}
        app['name'] = device['name']
        app['image'] = validic.validic_defaults['url'] + device['logo_url']
        app['unsync_url'] = device['unsync_url'] + redirect_param
        app['refresh_url'] = device['refresh_url'] + redirect_param

        synced_apps.append(app)

    for device in apps['unsynced']:
        app = {}
        app['name'] = device['name']
        app['image'] = validic.validic_defaults['url'] + device['logo_url']
        app['sync_url'] = device['sync_url'] + redirect_param

        unsynced_apps.append(app)

    # 23andMe Device
    try:
        genetic_profile = GeneticProfile.objects.get(user=owner)
    except GeneticProfile.DoesNotExist:
        genetic_profile = None

    ttam_app = {}
    ttam_app['name'] = '23andMe'
    ttam_app['image'] = 'http://upload.wikimedia.org/wikipedia/en/3/39/23andme_logo.png'

    if genetic_profile:
        ttam_app['unsync_url'] = reverse('unsync_ttam_device')
        ttam_app['refresh_url'] = ''

        synced_apps.append(ttam_app)
    else:
        ttam_app['sync_url'] = ttam_client.LOGIN_URL

        unsynced_apps.append(ttam_app)

    context['synced_apps'] = synced_apps
    context['unsynced_apps'] = unsynced_apps
    return render_to_response(template, context, RequestContext(request))


def blood_test(request, template='track/blood_test.html'):
    """
    shows blood test

    **Context**
    ``RequestContext``

    **Template:**
    :template:`track/blood_test.html`
    """
    context = {}
    return render_to_response(template, context, RequestContext(request))


@login_required
def steps(request, patient_id=None, template='track/profile.html'):
    """
    shows user's steps activity
    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    **Template:**
    :template:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(request, "steps", patient=patient)
    else:
        chart_data = get_activity_chart_data(request, "steps")

    if request.is_ajax():
        data = json.dumps(chart_data)

        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = str(chart_data)
        return render_to_response(template, context, RequestContext(request))


@login_required
def floor(request, patient_id=None, template='track/profile.html'):
    """
    shows user's floor activity

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    **Template:**
    :model:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(request, "floor", patient=patient)
    else:
        chart_data = get_activity_chart_data(request, "floor")

    if request.is_ajax():
        data = json.dumps(chart_data)

        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = str(chart_data)
        return render_to_response(template, context, RequestContext(request))


@login_required
def calorie(request, patient_id=None, template='track/profile.html'):
    """
    shows user's calorie activity

    **Context**
    ``RequestContext``

    ``user``
    :model:`auth.User`

    **Template:**
    :template:`track/profile.html`
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    if patient:
        chart_data = get_activity_chart_data(request, "calorie", patient=patient)
    else:
        chart_data = get_activity_chart_data(request, "calorie")

    if request.is_ajax():
        data = json.dumps(chart_data)

        return HttpResponse(data)
    else:
        context = {}
        context["chart_data"] = str(chart_data)
        return render_to_response(template, context, RequestContext(request))


def get_activity_chart_data(request, activity_type="", date_field="date", default_filter="hourly", operation="sum", patient=None):
    """
    Common method for fetching activity data steps/floor/calorie
    and returning a dictionary which will be used as an
    object literal data for javascript method highcharts()

    TODO:
    Utilize aggregation model management method
    for displaying as yAxis
    """
    activity_class = None
    field_name = ""
    timeline = []
    time_value = []

    if activity_type == "steps":
        activity_class = Steps
        field_name = "steps"
    elif activity_type == "floor":
        activity_class = Floor
        field_name = "floors"
    elif activity_type == "calorie":
        activity_class = Calorie
        field_name = "calories"
    elif activity_type == "bone_mass":
        activity_class = BoneMass
        field_name = "bone_mass"
    elif activity_type == "body_water":
        activity_class = BodyWater
        field_name = "body_water"
    elif activity_type == "body_mass_index":
        activity_class = BodyMassIndex
        field_name = "bmi"
    elif activity_type == "visceral_fat":
        activity_class = VisceralFatRating
        field_name = "visceral_fat"
    elif activity_type == "lean_body_mass":
        activity_class = LeanBodyMass
        field_name = "lean_body_mass"
    elif activity_type == "body_fat":
        activity_class = BodyFat
        field_name = "body_fat"
    elif activity_type == "muscle_mass":
        activity_class = MuscleMass
        field_name = "muscle_mass"

    date_filter = request.GET['date_range'] if 'date_range' in request.GET else default_filter

    if patient:
        activities = activity_class.objects.filter(user=patient).date_filter(date_field, date_filter).order_by(date_field)
    else:
        activities = activity_class.objects.filter(user=request.user).date_filter(date_field, date_filter).order_by(date_field)

    refined_data = generate_series_data(activities, date_field, date_filter, field_name, operation, True)

    for data in refined_data:
        timeline.append(data[0])
        time_value.append(data[1])

    # chart_data = {
    #     "chart": {
    #         "type": "column"
    #     },
    #     "title": {
    #         "text": activity_class._meta.verbose_name
    #     },
    #     "subtitle": {
    #         "text": ""
    #     },
    #     "colors": [
    # "#FD7360"
    #     ],
    #     "legend": {
    #         "layout": "horizontal",
    #         "align": "right",
    #         "verticalAlign": "top",
    #         "x": -100,
    #         "y": 100,
    #         "floating": "true",
    #         "borderWidth": 1,
    # "backgroundColor": "#EEEEEE",
    #         "shadow": "true"
    #     },
    #     "series": [
    #         {
    #         "name": key_value,
    #         "data": key_data,
    #         }
    #     ],
    #     "xAxis": {
    #         "type": "datetime",
    #         "dateTimeLabelFormats": {
    #             "hour": "%H:%M",
    #             "day": "%e. %b",
    #             "week": "%e. %b",
    #             "month": "%b \ %y",
    #             "year": "%Y"
    #         }
    #     },
    #     "yAxis": {
    #         "min": 0,
    #         "title": {
    #             "text": activity_class.__name__
    #         },
    #         "labels": {
    #             "overflow": 'justify'
    #         }
    #     },
    #     "tooltip": {
    #         "valueSuffix": field_name
    #     },
    # }

    return render_chart_data(activity_class._meta.verbose_name, activity_class.__name__, timeline, time_value)


def add_patient_formset(request):
    """
    Handles formset for adding of patients
    """

    class RequiredFormSet(BaseFormSet):

        def __init__(self, *args, **kwargs):
            super(RequiredFormSet, self).__init__(*args, **kwargs)
            for form in self.forms:
                form.empty_permitted = False

    AddPatientFormSet = formset_factory(AddPatientForm, max_num=10, formset=RequiredFormSet)
    AddPatientFormSet.form = staticmethod(curry(AddPatientForm, physician=request.user))
    if request.method == 'POST':
        add_patient_formset = AddPatientFormSet(data=request.POST)

        if add_patient_formset.is_valid():
            error = None
            for form in add_patient_formset:
                error = form.save()
                if error:
                    break
            return error
        else:
            return add_patient_formset.errors
    else:
        add_patient_formset = AddPatientFormSet()
    return add_patient_formset


def add_patients(request):
    """
    Enables providers to add patients using email
    """

    if not request.is_ajax():
        return HttpResponseNotFound()

    errors = add_patient_formset(request)
    return HttpResponse(simplejson.dumps(errors), mimetype="application/json")


@group_required('telemed')
def roster(request, template='track/patient_roster.html'):
    """
    Shows page which displays the patient roster of the physician user

    TODO:
    * get list of patients
    * on click of patient, go to individual track profile
    """
    context = {}
    physician = request.user  # parent model has changed

    try:
        subscription = PhysicianSubscriptions.objects.filter(patient=physician).latest('subscription_date')
        if subscription and subscription.payment_status == "P":
            context['provider_subscription'] = True
    except:
        pass

    if not context.get('provider_subscription'):
        try:
            provider = User.objects.get(username="prevently")
            if provider:
                plan = Plan.objects.get(user=provider)
                context['plan'] = True
        except (User.DoesNotExist, Plan.DoesNotExist):
            pass
    patient_plan = telmed_sub_mods.PatientPlan.objects.filter(active=True,
                                                              patientplanprovider__active=True,
                                                              patientplanprovider__provider=physician)
    patients = patient_plan.values('user')
    context['patients'] = User.objects.filter(id__in=patients)

    context['formset'] = add_patient_formset(request)
    context.update(csrf(request))

    logging.debug("Patient/s %s for physician %s" % (
        context['patients'],
        physician)
    )

    return render_to_response(template, context, RequestContext(request))


@group_required('telemed')
def roster_subscription(request):
    """
    Subscribes providers to enable them to add patients using email
    """
    context = {}
    DESCRIPTION = "Monthly Subscription"

    cc_form = CreditCardForm(request.POST or None)
    context['description'] = DESCRIPTION
    context['cc_form'] = cc_form

    try:
        provider = User.objects.get(username="prevently")
        if provider:
            plan = Plan.objects.get(user=provider)
            custom = {
                'plan': plan.id,
                'subscriber': request.user.userprofile.id
            }
            context['plan'] = True
            context['provider_id'] = provider.id
            context['amount'] = plan.amount

    except (User.DoesNotExist, Plan.DoesNotExist):
        custom = provider = plan = None

    if request.POST and cc_form.is_valid():
        credit_card = CreditCard(**cc_form.cleaned_data)
        subscribe = get_gateway('stripe')

        try:
            subscribe.validate_card(credit_card)
        except CardNotSupported:
            return HttpResponse("Credit Card not supported!")

        options = {'plan_id': plan.plan_id}
        response = subscribe.recurring(credit_card, options)

        if response['status'] == 'SUCCESS':
            subscription = PhysicianSubscriptions.objects.create(
                physician=provider,
                patient=request.user,
                subscription_date=datetime.datetime.now(),
                amount=plan.amount,
                payment_method='CC',
                payment_status='P',
                customer_id=response['response']['id'],
            )

            messages.info(request, "User is now subscribed.")
            return HttpResponseRedirect(reverse('roster'))
        else:
            messages.error(request, "An error has occured!")
    else:
        messages.error(request, cc_form.errors.get("__all__"))

    return render_to_response("accounts/subscription_details.html", context, RequestContext(request))


def roster_cancel(request):
    """
    Removes current subscription of provider
    """
    context = {}
    provider = subscription = None
    try:
        provider = User.objects.get(username="prevently")
        subscription = PhysicianSubscriptions.objects.filter(physician=provider, patient=request.user).latest('subscription_date')
    except (User.DoesNotExist, Plan.DoesNotExist):
        messages.add_message(request, messages.ERROR, "Unable to find details regarding this subscription.")

    if provider:
        try:
            subscribe = get_gateway('stripe')
            response = subscribe.cancel_recurring(subscription.customer_id)

            if response['status'] == 'SUCCESS':
                subscription.payment_status = 'C'
                subscription.save()
                messages.add_message(request, messages.SUCCESS, "Your subscription has been successfully cancelled.")
            else:
                messages.add_message(request, messages.ERROR, response['response'])
        except:
            messages.add_message(request, messages.ERROR, response['response'])

    return HttpResponseRedirect(reverse('roster'))


def check_mailed(request):
    if not request.is_ajax():
        return HttpResponseNotFound()

    subprocess.call(["python", "manage.py", "send_mail"])
    return HttpResponse()


@group_required('telemed')
def scales(request, patient_id=None, template='track/sliding_scales.html'):
    """
    Shows page which displays a series of sliding scale thresholds
    for a patient of a physician
    """
    context = {}
    physician = request.user

    scales = (
        ('weight_scale', BodyWeightScale),
        ('calories_burned_scale', CaloriesScale),
        ('calorie_intake_scale', CalorieIntakeScale),
        ('systolic_bp_scale', SystolicBloodPressureScale),
        ('diastolic_bp_scale', DiastolicBloodPressureScale),
        ('sleep_scale', SleepScale),
        ('step_scale', StepScale),
        ('blood_glucose_scale', BloodGlucoseScale)
    )

    if request.is_ajax():
        scale_name = request.POST.get('scale_name')
        patient_id = request.POST.get('patient_id')
        scale_class = None

        for scale in scales:
            if scale_name == scale[0]:
                scale_class = scale[1]
                break
        try:
            patient = User.objects.get(pk=patient_id)

            try:
                scale = scale_class.objects.get(
                    user=physician,
                    patient=patient
                )

                if request.POST.get('action') == 'set_slider_values':
                    scale_value = json.loads(request.POST.get('scale_value'))
                    update_scale_min_max(scale, scale_value)
                elif request.POST.get('action') == 'set_time_component':
                    try:
                        time_component = int(request.POST.get('time_component'))
                        scale.time_component = time_component
                        scale.save()
                    except ValueError as e:
                        HttpResponse(str(e))
            except MultipleObjectsReturned:
                logger.debug("Query returned multiple objects.")
        except User.DoesNotExist:
            logger.debug("User does not exist.")

    else:
        try:
            patient = User.objects.get(pk=patient_id)
            context['patient'] = patient

            sliders = (
                (
                    get_current_weight(patient),
                    'weight-range',
                    'weight-slider',
                    '',
                    'Current Weight:',
                    'lbs',
                    0,
                    400,
                    1
                ),
                (
                    get_current_calories_burned(patient),
                    'calories-burned-range',
                    'calories-burned-slider',
                    'calories-burned-days',
                    'Current Calories Burned:',
                    'cal',
                    0,
                    1000,
                    1
                ),
                (
                    get_current_calorie_intake_average(patient),
                    'calorie-intake-range',
                    'calorie-intake-slider',
                    'calorie-intake-days',
                    'Current Calorie Intake:',
                    'cal',
                    0,
                    1000,
                    1
                ),
                (
                    get_current_systolic_bp(patient),
                    'systolic-bp-range',
                    'systolic-bp-slider',
                    'systolic-bp-days',
                    'Current Systolic Blood Pressure:',
                    'mmHg',
                    0,
                    250,
                    1
                ),
                (
                    get_current_diastolic_bp(patient),
                    'diastolic-bp-range',
                    'diastolic-bp-slider',
                    'diastolic-bp-days',
                    'Current Diastolic Blood Pressure:',
                    'mmHg',
                    0,
                    250,
                    1
                ),
                (
                    get_current_sleep_average(patient, physician),
                    'sleep-range',
                    'sleep-slider',
                    'sleep-days',
                    'Current Average Sleep Hours:',
                    'hrs',
                    0,
                    12,
                    1
                ),
                (
                    get_current_step_average(patient, physician),
                    'step-range',
                    'step-slider',
                    'step-days',
                    'Current Average Steps:',
                    'steps',
                    0,
                    15000,
                    1
                ),
                (
                    get_current_blood_glucose(patient),
                    'blood-glucose-range',
                    'blood-glucose-slider',
                    'blood-glucose-days',
                    'Current Blood Glucose:',
                    'mmol/L',
                    0,
                    30,
                    1
                )
            )

            try:
                scales_context = []
                for index, scale in enumerate(scales):
                    slider = sliders[index]
                    scale_dict = {}

                    scale_instance, create = scale[1].objects.get_or_create(
                        user=physician,
                        patient=patient
                    )
                    if create:
                        calculate_scale_default(scale_instance)

                    scale_dict['scale'] = scale_instance
                    scale_dict['current_value'] = slider[0]
                    scale_dict['range'] = slider[1]
                    scale_dict['slider'] = slider[2]
                    scale_dict['days'] = slider[3]
                    scale_dict['header'] = slider[4]
                    scale_dict['label'] = slider[5]
                    scale_dict['min'] = slider[6]
                    scale_dict['max'] = slider[7]
                    scale_dict['step'] = slider[8]

                    scales_context.append(scale_dict)

                context['scales_context'] = scales_context

                return render_to_response(template, context, RequestContext(request))

            except MultipleObjectsReturned:
                logger.debug("Query returned multiple objects.")

        except User.DoesNotExist:
            logger.debug("User does not exist.")

    return HttpResponse('')


@login_required
def device_settings(request, template="track/device_settings.html"):
    """
    Shows page to manage device accounts
    """

    context = {}
    context['accounts'] = request.user.userprofile.deviceaccount_set.all()
    providers = context['accounts'].values_list('provider', flat=True)
    #all_providers = list(constants.DEVICE_PROVIDERS)
    all_providers = [constants.DEVICE_PROVIDERS[0]]  # only fitbit for now
    for provider in all_providers:
        if provider[0] in providers:
            all_providers.remove(provider)
    context['other_accounts'] = all_providers

    # TODO: fix when there are additional providers aside from fitbit
    client = FitBitOAuthClient(request)
    context["connect_link"] = client.get_full_authorize_url()

    return render_to_response(template, context, RequestContext(request)
                              )


@login_required
def remove_device_account(request):
    """
    Ajax function that unlinks device accounts
    """
    if not request.is_ajax():
        return HttpResponseNotFound()

    error = ""
    try:
        account = DeviceAccount.objects.get(id=request.GET.get('id'))
        for device in request.user.userprofile.devices.filter(provider=account.provider):
            request.user.userprofile.devices.remove(device)
        account.delete()
    except DeviceAccount.DoesNotExist as e:
        error = str(e)

    return HttpResponse(error)


@csrf_exempt
def food_chart(request, template="track/food_chart.html"):
    """
    Displays food chart
    """
    date = datetime.date.today()
    current_week = OrderedDict()
    context = {}
    dates = []
    timedelta_input = 0
    time_guide = []
    curr_offset = '+00:00'

    [time_guide.append(str(each_hour)) for each_hour in range(0, 24)]

    if request.is_ajax():
        if request.GET:
            date = dateutil.parser.parse(str(request.GET.get('date'))).date()
            timedelta_input = int(request.GET.get('time_delta'))

        start_of_currweek = get_week_startdate(date, timedelta_input)  # get what week to display else returns start date of current week
        curr_week_dates = [start_of_currweek + datetime.timedelta(days=i) for i in range(7)]

        user_foodintake = request.user.userprofile and None
        foodintake = FoodIntake.objects.filter(userprofile=request.user.userprofile)

        if foodintake:
            existing_foodintake = foodintake.latest('datetime_taken')
            curr_offset = existing_foodintake.utc_offset

        for weekday in range(7):
            for time in time_guide:
                time_foods = []
                hour_gmt = "AM"

                for current_date in curr_week_dates:
                    used_date = []
                    local_time = datetime.datetime.strptime(str(current_date) + ' ' + time + ':00:00', '%Y-%m-%d %H:%M:%S')
                    utc = local_time - datetime.timedelta(hours=int(curr_offset[0:3]))

                    ufi = FoodIntake.objects.filter(
                        datetime_taken__startswith=utc.strftime('%Y-%m-%d'),
                        datetime_taken__regex=utc.strftime('%H') + ':.+:.+',
                        userprofile=request.user.userprofile
                    ).order_by('datetime_taken')

                    for data in ufi:
                        used_date_details = []
                        used_date_details.append(data.food.name)
                        used_date_details.append('%.2f' % data.food.serving)
                        used_date_details.append('%.2f' % get_calories(data.id))
                        used_date_details.append(data.get_image_url())
                        used_date.append(used_date_details)

                    time_foods.append({current_date.strftime('%b. %d, %Y'): used_date})

                if int(time) > 12:
                    time = str(int(time) - 12)
                    hour_gmt = "PM"

                if int(time) == 12:
                    hour_gmt = "PM"

                if time == '0':
                    time = str(int(time) + 12)
                    hour_gmt = "AM"

                current_week[time + ":00 " + hour_gmt] = time_foods

        for current_date in curr_week_dates:
            date_entry = []
            calories_of_day = get_total_calories(request.user, current_date, int(curr_offset[0:3]))

            date_entry.append(current_date.strftime('%b. %d, %Y'))
            date_entry.append('%.2f' % calories_of_day)
            date_entry.append(current_date.strftime("%A"))

            dates.append(date_entry)

        current_week['dates'] = dates
        current_week['week_num'] = [start_of_currweek.isocalendar()[1]]

        context = {"current_week": current_week}
        data = simplejson.dumps(context)

        return HttpResponse(data, mimetype="application/json")


@login_required
def get_fitness_calories(request, patient_id=None, mimetype="application/json"):
    """
    Returns fitness calories data
    """
    try:
        patient = User.objects.get(pk=patient_id)
    except User.DoesNotExist:
        patient = None

    context = {}

    if request.is_ajax():
        context = {}
        context['checker'] = 0
        # exclude_activities = [
        #     'Running',
        #     'Swimming',
        #     'Cycling',
        #     'Elliptical'
        # ]
        # running_activities = [
        #     'Running',
        #     'Jogging',
        #     'Walking',

        # ]

        timeline, running, swimming, jogging = ([] for i in range(4))

        activity = request.GET.get('activity')

        date_filter = request.GET['date_range'] if 'date_range' in request.GET else datetime.time()
        date_range = request.GET.get("date_range", "daily")
        if patient:
            fitness_activities = Fitness.objects.filter(user=patient).date_filter('timestamp', date_range).order_by('date_checked').order_by('timestamp')
        else:
            fitness_activities = Fitness.objects.filter(user=request.user).date_filter('timestamp', date_range).order_by('date_checked').order_by('timestamp')

        timeline = generate_series_data(
            fitness_activities.filter(activity_type__icontains=activity),
            'timestamp',
            date_range,
            'calories',
            'average',
            True
        )

        if activity == 'Others':
            timeline = generate_series_data(
                fitness_activities.exclude(Q(activity_type__icontains='jog')
                                           | Q(activity_type__icontains='walk')
                                           | Q(activity_type__icontains='run')
                                           | Q(activity_type__icontains='swim')
                                           | Q(activity_type__icontains='cyc')
                                           | Q(activity_type__icontains='elliptical')),
                'timestamp',
                date_range,
                'calories',
                'average',
                True
            )

        if activity == 'Run':
            timeline = generate_series_data(
                fitness_activities.filter(Q(activity_type__icontains='run')
                                          | Q(activity_type__icontains='jog')
                                          | Q(activity_type__icontains='walk')),
                'timestamp',
                date_range,
                'calories',
                'average',
                True
            )

        context['xaxis'] = [current_time[0] for current_time in timeline]
        context['yaxis_title'] = 'Meters'
        context['yaxis_type'] = 'linear'
        context['series'] = [
            {
                'name': activity,
                'data': get_fitness_activity_data(
                    fitness_activities,
                    activity,
                    date_range,
                    context['xaxis']
                )
            }
        ]

        for data in context['series']:
            if(data['data']):
                context['checker'] = 1

        return HttpResponse(simplejson.dumps(context), mimetype='text/json')


def get_fitness_activity_data(fitness_activities, activity, date_range, date_filter):

    activity_dict = {}
    activity_list = []
    # exclude_activities = ['Running', 'Swimming', 'Cycling', 'Elliptical']
    # running_activities = ['Running', 'Run']

    activity_calories = generate_series_data(
        fitness_activities.filter(activity_type__icontains=activity),
        'timestamp',
        date_range,
        'calories',
        'average',
        True
    )
    activity_distance = generate_series_data(
        fitness_activities.filter(activity_type__icontains=activity),
        'timestamp',
        date_range,
        'distance',
        'average',
        True
    )
    activity_duration = generate_series_data(
        fitness_activities.filter(activity_type__icontains=activity),
        'timestamp',
        date_range,
        'duration',
        'average',
        True
    )

    if activity == "Others":
        activity_calories = generate_series_data(
            fitness_activities.exclude(Q(activity_type__icontains='jog')
                                       | Q(activity_type__icontains='walk')
                                       | Q(activity_type__icontains='run')
                                       | Q(activity_type__icontains='swim')
                                       | Q(activity_type__icontains='cyc')
                                       | Q(activity_type__icontains='elliptical')),
            'timestamp',
            date_range,
            'calories',
            'average',
            True
        )
        activity_distance = generate_series_data(
            fitness_activities.exclude(Q(activity_type__icontains='jog')
                                       | Q(activity_type__icontains='walk')
                                       | Q(activity_type__icontains='run')
                                       | Q(activity_type__icontains='swim')
                                       | Q(activity_type__icontains='cyc')
                                       | Q(activity_type__icontains='elliptical')),
            'timestamp',
            date_range,
            'distance',
            'average',
            True
        )
        activity_duration = generate_series_data(
            fitness_activities.exclude(Q(activity_type__icontains='jog')
                                       | Q(activity_type__icontains='walk')
                                       | Q(activity_type__icontains='run')
                                       | Q(activity_type__icontains='swim')
                                       | Q(activity_type__icontains='cyc')
                                       | Q(activity_type__icontains='elliptical')),
            'timestamp',
            date_range,
            'duration',
            'average',
            True
        )

    if activity == "Run":
        activity_calories = generate_series_data(
            fitness_activities.filter(Q(activity_type__icontains=activity)
                                      | Q(activity_type__icontains='jog')
                                      | Q(activity_type__icontains='walk')),
            'timestamp',
            date_range,
            'calories',
            'average',
            True
        )
        activity_distance = generate_series_data(
            fitness_activities.filter(Q(activity_type__icontains=activity)
                                      | Q(activity_type__icontains='jog')
                                      | Q(activity_type__icontains='walk')),
            'timestamp',
            date_range,
            'distance',
            'average',
            True
        )
        activity_duration = generate_series_data(
            fitness_activities.filter(Q(activity_type__icontains=activity)
                                      | Q(activity_type__icontains='jog')
                                      | Q(activity_type__icontains='walk')),
            'timestamp',
            date_range,
            'duration',
            'average',
            True
        )

    for curr_time in date_filter:
        activity_dict[curr_time] = {}

        for distance in activity_distance:
            if(curr_time == distance[0]):
                activity_dict[curr_time]['y'] = distance[1]
        for calorie in activity_calories:
            if(curr_time == calorie[0]):
                activity_dict[curr_time]['Calories'] = calorie[1]
        for duration in activity_duration:
            if(curr_time == duration[0]):
                activity_dict[curr_time]['Duration'] = duration[1]

    return activity_dict.values()


def callback_genetics(request):
    """
    TODO
    * Get authorization code for user after login on 23andme
    * Use authorization code to get access token
    * Save access token to request.session
    * Use access token for api calls
    """
    c = ttam_client.OAuthClient()
    if 'code' in request.GET:
        code = request.GET["code"]
        logger.debug("code: %s" % code)

        logger.debug("fetching token...")

        (access_token, refresh_token) = c.get_token(code)
        logger.debug("access_token: %s refresh_token: %s" % (access_token, refresh_token))

        logger.debug("refreshing token...")

        (access_token, refresh_token) = c.refresh_token(refresh_token)
        logger.debug("access_token: %s refresh_token: %s" % (access_token, refresh_token))

        request.session[ttam_client.OAUTH_KEY] = access_token

        c = ttam_client.OAuthClient(request.session[ttam_client.OAUTH_KEY])
        names_json = c.get_names()

        try:
            genetic_profile, created = GeneticProfile.objects.get_or_create(
                user=request.user,
                profile_id=names_json['profiles'][0]['id']
            )
        except KeyError:
            pass

        return HttpResponseRedirect(reverse('track_settings'))


def unsync_ttam_device(request):
    """
    Unsyncs 23andMe device
    """
    try:
        GeneticProfile.objects.get(user=request.user).delete()
    except GeneticProfile.DoesNotExist:
        pass

    return HttpResponseRedirect(reverse('track_settings'))
