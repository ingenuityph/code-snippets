import requests
# Get the token using a POST request and a code

import settings

SCOPE = 'basic names rs53576 rs1815739 rs6152 rs1800497 rs1805007 rs9939609 rs662799 rs7495174 rs7903146 rs12255372 rs1799971 rs17822931 rs4680 rs1333049 rs1801133 rs1051730 rs3750344 rs4988235'

# leave these alone
BASE_URL = 'https://api.23andme.com/1/'
LOGIN_URL = 'https://api.23andme.com/authorize/?redirect_uri=%s&response_type=code&client_id=%s&scope=%s' % (settings.CALLBACK_URL, settings.CLIENT_ID, SCOPE)
OAUTH_KEY = '23andme_access_token'


class OAuthClient(object):

    def __init__(self, access_token=None):
        self.access_token = access_token

    def get_token(self, authorization_code):
        parameters = {
            'client_id': settings.CLIENT_ID,
            'client_secret': settings.CLIENT_SECRET,
            'grant_type': 'authorization_code',
            'code': authorization_code,  # the authorization code obtained above
            'redirect_uri': settings.CALLBACK_URL,
            'scope': SCOPE,
        }
        response = requests.post(
            'https://api.23andme.com/token/',
            data=parameters
        )

        if response.status_code == 200:
            return (response.json()['access_token'], response.json()['refresh_token'])
        else:
            response.raise_for_status()

    def refresh_token(self, refresh_token):
        parameters = {
            'client_id': settings.CLIENT_ID,
            'client_secret': settings.CLIENT_SECRET,
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token,
            'redirect_uri': settings.CALLBACK_URL,
            'scope': SCOPE,
        }
        response = requests.post(
            'https://api.23andme.com/token/',
            data=parameters
        )

        if response.status_code == 200:
            self.access_token = response.json()['access_token']
            return (response.json()['access_token'], response.json()['refresh_token'])
        else:
            response.raise_for_status()

    def _get_resource(self, resource):
        if self.access_token is None:
            raise Exception('access_token cannot be None')

        headers = {'Authorization': 'Bearer %s' % self.access_token}
        url = '%s%s' % (BASE_URL, resource)
        response = requests.get(
            url,
            headers=headers,
            verify=False,
        )
        if response.status_code == 200:
            return response.json()
        else:
            response.raise_for_status()

    # User
    def get_user(self):
        return self._get_resource('user/')

    def get_names(self):
        return self._get_resource('names/')

    def get_profiles(self):
        return self._get_resource('profiles/')

    # Genetics
    def get_genotype(self, locations):
        return self._get_resource('genotypes/?locations=%s' % locations)

    # Health
    def get_risks(self, profile_id):
        resource = 'risks/%s' % profile_id
        return self._get_resource(resource)

    def get_carriers(self, profile_id):
        resource = 'carriers/%s' % profile_id
        return self._get_resource(resource)

    def get_drug_responses(self, profile_id):
        resource = 'drug_responses/%s' % profile_id
        return self._get_resource(resource)

    def get_traits(self, profile_id):
        resource = 'traits/%s' % profile_id
        return self._get_resource(resource)
