import json
import logging
import settings
import urllib
import urllib2
import dateutil.parser

from decimal import Decimal
from django.core.exceptions import MultipleObjectsReturned

from accounts.models import ValidicProfile

logger = logging.getLogger(__name__)


class Validic:

    """
    Validic utility class
    """
    headers = {
        'Content-Type': 'application/json',
    }
    organization_values = {}
    validic_defaults = {
        'url': 'https://api.validic.com',
        'version': 'v1'
    }

    def __init__(self, app_id, secret, access_token):
        self.organization_values = {
            'app_id': app_id,
            'secret': secret,
            'access_token': access_token
        }

    # API methods
    # Organization Methods
    def get_organization_overview(self):
        """
        Gets an overview of an Organization and it's Child Organizations
        """
        logger.info("Get overview of organization")

        end_point = '%s/%s/organizations/%s.json' % (
            self.validic_defaults['url'],
            self.validic_defaults['version'],
            self.organization_values['app_id']
        )
        params = {
            'access_token': self.organization_values['access_token']
        }

        params_string = urllib.urlencode(params)

        try:
            response = urllib2.urlopen(end_point + '?' + params_string).read()
        except urllib2.HTTPError as error:
            response = error.read()

        response_dict = json.loads(response)

        prettify_response = json.dumps(
            response_dict,
            indent=4,
            separators=(',', ': ')
        )
        logger.debug("%s" % prettify_response)

        return response_dict

    # User Methods
    def get_users(self, status):
        """
        Base method for getting of users with specific status
        """
        logger.info("Get %s users" % status)

        end_point = '%s/%s/organizations/%s/users.json' % (
            self.validic_defaults['url'],
            self.validic_defaults['version'],
            self.organization_values['app_id']
        )
        params = {
            'access_token': self.organization_values['access_token'],
            'status': status
        }

        params_string = urllib.urlencode(params)
        response = urllib.urlopen(end_point + '?' + params_string).read()

        response_dict = json.loads(response)
        prettify_response = json.dumps(
            response_dict,
            indent=4,
            separators=(',', ': ')
        )
        logger.debug("%s" % prettify_response)

        return response_dict

    def get_provisioned_users(self):
        """
        Gets all provisioned users for the organization with no synced apps
        """
        return self.get_users('provisioned')

    def get_active_users(self):
        """
        Gets all active users for the organization with synced apps within the last 90 days
        """
        return self.get_users('active')

    def get_inactive_users(self):
        """
        Gets all inactive users for the organization with synced apps within the last 90 days
        """
        return self.get_users('inactive')

    def get_suspended_users(self):
        """
        Gets all suspended users for the organization
        """
        return self.get_users('suspended')

    def provision_user(self, user):
        """
        Provisions a user in a unique way to provide user authentication
        and data linkage between this system in Validic
        """
        logger.info("Provision user %s" % user.username)

        end_point = '%s/%s/organizations/%s/users.json' % (
            self.validic_defaults['url'],
            self.validic_defaults['version'],
            self.organization_values['app_id']
        )
        params = {
            'user': {
                'uid': str(user.id) + '-' + settings.VALIDIC_SUFFIX
            },
            'access_token': self.organization_values['access_token']
        }

        data = json.dumps(params)
        request = urllib2.Request(end_point, data, self.headers)

        try:
            response = urllib2.urlopen(request).read()
        except urllib2.HTTPError as error:
            # response = error.read()
            logger.error("Validic Error: %s" % error)
            response = dict()

        response_dict = json.loads(response)
        prettify_response = json.dumps(
            response_dict,
            indent=4,
            separators=(',', ': ')
        )
        logger.debug("%s" % prettify_response)

        try:
            ValidicProfile.objects.create(
                user=user,
                validic_id=response_dict['user']['_id'],
                access_token=response_dict['user']['access_token']
            )
        except KeyError:
            logger.debug("Key of response dictionary does not exist.")

        return response_dict

    def refresh_user_access_token(self, user, validic_id):
        """
        Refreshes user's access token
        """
        logger.info("Refresh access token for user %s" % user.username)

        end_point = '%s/%s/organizations/%s/users/%s/refresh_token.json' % (
            self.validic_defaults['url'],
            self.validic_defaults['version'],
            self.organization_values['app_id'],
            validic_id
        )
        params = {
            'access_token': self.organization_values['access_token'],
        }

        params_string = urllib.urlencode(params)
        response = urllib.urlopen(end_point + '?' + params_string).read()

        response_dict = json.loads(response)
        prettify_response = json.dumps(
            response_dict,
            indent=4,
            separators=(',', ': ')
        )
        logger.debug("%s" % prettify_response)

        try:
            validic_profile, create = ValidicProfile.objects.get_or_create(
                user=user,
                validic_id=validic_id
            )

            validic_profile.access_token = response_dict['user']['authentication_token']
            validic_profile.save()
        except MultipleObjectsReturned:
            logger.debug("Query returned multiple objects.")

        return response_dict

    def get_validic_profile(self, user):
        """
        Gets validic profile for user
        """
        logger.info("Get validic profile for %s" % user.username)

        try:
            validic_profile = ValidicProfile.objects.get(user=user)
        except ValidicProfile.DoesNotExist:
            self.provision_user(user)
            validic_profile = ValidicProfile.objects.get(user=user)
        logger.debug("Validic Profile %s" % validic_profile)

        return validic_profile

    def delete_validic_user(self, validic_id):
        """
        Deletes a validic user
        """
        logger.info("Delete validic user %s" % validic_id)

        end_point = '%s/%s/organizations/%s/users/%s' % (
            self.validic_defaults['url'],
            self.validic_defaults['version'],
            self.organization_values['app_id'],
            validic_id
        )
        params = {
            'access_token': self.organization_values['access_token']
        }

        data = json.dumps(params)

        opener = urllib2.build_opener(urllib2.HTTPHandler)
        request = urllib2.Request(end_point, data, self.headers)
        request.get_method = lambda: 'DELETE'
        url = opener.open(request)

        logger.debug("Opening url: {0}".format(url))

    # App methods
    def get_app_listing(self, validic_profile, source=None):
        """
        Gets the listing of all apps and its appropriate sync, refresh,
        and unsync URLs using the Validic Marketplace
        """
        logger.info("Get all app listing for user %s" % validic_profile.user.username)

        end_point = '%s/%s/organizations/%s/apps.json' % (
            self.validic_defaults['url'],
            self.validic_defaults['version'],
            self.organization_values['app_id']
        )
        params = {
            'authentication_token': validic_profile.access_token,
            'access_token': self.organization_values['access_token']
        }
        if source:
            params['source'] = source
            params['expanded'] = 1

        params_string = urllib.urlencode(params)
        response = urllib.urlopen(end_point + '?' + params_string).read()

        response_dict = dict()
        try:
            response_dict = json.loads(response)
        except ValueError, e:
            logger.error(e)

        return response_dict

    def get_apps(self, user, devices):
        """
        Gets synced and unsynced apps
        """
        apps = {
            'synced': [],
            'unsynced': []
        }
        all_apps_listing = self.get_app_listing(user)
        for app in all_apps_listing['apps']:
            if app['subname'] in devices:
                if app['synced']:
                    apps['synced'].append(app)
                else:
                    apps['unsynced'].append(app)
        return apps

    def get_app_refresh_url(self, user, source):
        """
        Gets the refresh url of the specific source(ex. Fitbit, Nike Fuel, ...)
        """
        all_apps_listing = self.get_app_listing(user, source)
        for app in all_apps_listing['apps']:
            if app['name'].lower() == source:
                return app['refresh_url']
        return ''

    # Get Methods
    def get_data(self, user, action, source=None, start_date=None):
        """
        Base method for getting user's data for actions: (fitness, routine, nutrition, sleep,
        weight, diabetes, biometrics, tobacco_cessation)
        """
        from track.models import SyncLog

        validic_profile = self.get_validic_profile(user)

        end_point = '%s/%s/organizations/%s/users/%s/%s/latest.json' % (
            self.validic_defaults['url'],
            self.validic_defaults['version'],
            self.organization_values['app_id'],
            validic_profile.validic_id,
            action
        )
        params = {
            'access_token': self.organization_values['access_token']
        }
        if source:
            params['source'] = source
            params['expanded'] = 1
        if start_date:
            params['start_date'] = start_date

        params_string = urllib.urlencode(params)
        response = urllib.urlopen(end_point + '?' + params_string).read()

        response_dict = json.loads(response)
        data_list = response_dict[action]

        if data_list:
            sync_log, create = SyncLog.objects.get_or_create(
                user=user,
                action=action
            )
            for data in data_list:
                sync_log.timestamp = data['timestamp']
                sync_log.save()
                break

        return response_dict

    def get_weight(self, user, start_date=None, source=None):
        """
        Gets measurements associated with a user's weight and body mass
        """
        return self.get_data(user, 'weight', start_date=start_date, source=source)

    def get_routine(self, user, start_date=None, source=None):
        """
        Gets activities that occur regularly throughout the day,
        without the specific goal of exercise, for example calories burned
        and consumed, steps taken, stairs climbed
        """
        return self.get_data(user, 'routine', start_date=start_date, source=source)

    def get_sleep(self, user, start_date=None, source=None):
        """
        Gets measurements related to the length of time spent
        in various sleep cycles, as well as number of times
        woken during the night
        """
        return self.get_data(user, 'sleep', start_date=start_date, source=source)

    def get_fitness(self, user, start_date=None, source=None):
        """
        Gets measurements related activities that are
        undertaken with the express purpose of exercising
        """
        return self.get_data(user, 'fitness', start_date=start_date, source=source)

    def get_nutrition(self, user, start_date=None, source=None):
        """
        Gets activities related to calorie intake and consumption
        and nutritional information (such as fat, protein,
        carbohydrates, sodium, etc.)
        """
        return self.get_data(user, 'nutrition', start_date=start_date, source=source)

    def get_diabetes(self, user, start_date=None, source=None):
        """
        Get diabetes measurements comprised of a user's blood glucose and
        hormone levels related to diabetes treatment and management
        """
        return self.get_data(user, 'diabetes', start_date=start_date, source=source)

    def get_biometrics(self, user, start_date=None, source=None):
        """
        Get biometric measurements comprised of a user's biometric health
        data such as blood pressure, cholesterol, heart rate, and
        blood and hormone levels.
        """
        return self.get_data(user, 'biometrics', start_date=start_date, source=source)

    def get_tobacco_cessation(self, user, start_date=None, source=None):
        """
        Get measurements associated with a user going through a tobacco cessation program.
        """
        return self.get_data(user, 'tobacco_cessation', start_date=start_date, source=source)

    def get_all_records(self, user):
        """
        Get all records of the user
        """
        validic_profile = self.get_validic_profile(user)

        end_point = '%s/%s/organizations/%s/users/%s.json' % (
            self.validic_defaults['url'],
            self.validic_defaults['version'],
            self.organization_values['app_id'],
            validic_profile.validic_id
        )
        params = {
            'access_token': self.organization_values['access_token']
        }

        params_string = urllib.urlencode(params)
        response = urllib.urlopen(end_point + '?' + params_string).read()

        response_dict = json.loads(response)

        return response_dict

    # Set Methods
    def set_routine(self, user, data_list):
        """
        Sets calories burned, steps taken, and floors from Validic Sync
        """
        from track.models import (
            Steps,
            Floor,
            Calorie
        )

        for data in data_list:
            timestamp = dateutil.parser.parse(data['timestamp'])

            if data['steps']:
                steps = int(data['steps'])
                try:
                    steps_obj, steps_create = Steps.objects.get_or_create(
                        user=user,
                        date=timestamp.date(),
                        time=timestamp.time()
                    )
                    steps_obj.steps = steps
                    steps_obj.save()

                    logger.debug("Created Steps object: {0}".format(steps_obj))
                except MultipleObjectsReturned:
                    logger.debug("Query returned multiple objects.")

            if data['floors']:
                floors = int(data['floors'])
                try:
                    floors_obj, floors_create = Floor.objects.get_or_create(
                        user=user,
                        date=timestamp.date(),
                        time=timestamp.time()
                    )
                    floors_obj.floors = floors
                    floors_obj.save()

                    logger.debug("Created Floor object: {0}".format(floors_obj))
                except MultipleObjectsReturned:
                    logger.debug("Query returned multiple objects.")

            if data['calories_burned']:
                calories_burned = int(data['calories_burned'])
                try:
                    calories_obj, calories_create = Calorie.objects.get_or_create(
                        user=user,
                        date=timestamp.date(),
                        time=timestamp.time()
                    )
                    calories_obj.calories = calories_burned
                    calories_obj.save()

                    logger.debug("Created Calorie object: {0}".format(calories_obj))
                except MultipleObjectsReturned:
                    logger.debug("Query returned multiple objects.")

    def set_biometrics(self, user, data_list):
        """
        Sets systolic and diastolic blood pressure from Validic Sync
        """
        from track.models import BloodPressure

        for data in data_list:
            timestamp = dateutil.parser.parse(data['timestamp'])
            systolic = 0
            diastolic = 0
            if data['systolic']:
                systolic = int(data['systolic'])
            if data['diastolic']:
                diastolic = int(data['diastolic'])

            if systolic or diastolic:
                try:
                    blood_pressure, create = BloodPressure.objects.get_or_create(
                        user=user,
                        date_checked=timestamp
                    )
                    blood_pressure.systolic = systolic
                    blood_pressure.diastolic = diastolic
                    blood_pressure.save()

                    logger.debug("Created BloodPressure object: {0}".format(blood_pressure))
                except MultipleObjectsReturned:
                    logger.debug("Query returned multiple objects.")

    def set_weight(self, user, data_list):
        """
        Sets weight from Validic Sync
        """
        from track.models import BodyWeight, BodyMassIndex
        from track.utils import convert_weight

        for data in data_list:
            timestamp = dateutil.parser.parse(data['timestamp'])
            weight = 0
            bmi = 0
            if data['weight']:
                weight = convert_weight(float(data['weight']), 'kgs')

                try:
                    body_weight, create = BodyWeight.objects.get_or_create(
                        user=user,
                        date=timestamp.date()
                    )

                    body_weight.weight = weight
                    body_weight.time = timestamp.time()
                    body_weight.save()

                    logger.debug("Created BodyWeight object: {0}".format(body_weight))
                except MultipleObjectsReturned:
                    logger.debug("Query returned multiple objects.")

            if data['bmi']:
                bmi = float(data['bmi'])

                try:
                    body_mass_index, create = BodyMassIndex.objects.get_or_create(
                        user=user,
                        date_checked=timestamp
                    )
                    body_mass_index.bmi = bmi
                    body_mass_index.save()

                    logger.debug("Created BodyMassIndex object: {0}".format(body_mass_index))
                except MultipleObjectsReturned:
                    logger.debug("Query returned multiple objects.")

    def set_sleep(self, user, data_list):
        """
        Sets sleep from Validic Sync
        """
        from track.models import Sleep

        for data in data_list:
            timestamp = dateutil.parser.parse(data['timestamp'])
            utc_offset = data['utc_offset']
            source = data['source']
            source_name = data['source_name']
            last_updated = data['last_updated']

            total_sleep = 0
            awake = 0
            deep = 0
            light = 0
            rem = 0

            if data['total_sleep']:
                total_sleep = Decimal(data['total_sleep'])
            if data['awake']:
                awake = Decimal(data['awake'])
            if data['deep']:
                deep = Decimal(data['deep'])
            if data['light']:
                light = Decimal(data['light'])
            if data['rem']:
                rem = Decimal(data['rem'])

            if total_sleep or awake or deep or light or rem:
                try:
                    sleep, create = Sleep.objects.get_or_create(
                        user=user,
                        timestamp=timestamp,
                        utc_offset=utc_offset,
                        source=source,
                        source_name=source_name
                    )

                    sleep.total_sleep = total_sleep
                    sleep.awake = awake
                    sleep.deep = deep
                    sleep.light = light
                    sleep.rem = rem
                    sleep.last_updated = last_updated
                    sleep.save()

                    logger.debug("Created Sleep object: {0}".format(sleep))
                except MultipleObjectsReturned:
                    logger.debug("Query returned multiple objects.")

    def set_fitness(self, user, data_list):
        """
        Sets fitness from Validic Sync
        """
        from track.models import Fitness

        for data in data_list:
            timestamp = data['timestamp']
            utc_offset = data['utc_offset']
            source = data['source']
            source_name = data['source_name']
            last_updated = data['last_updated']

            activity_type = ''
            intensity = ''
            start_time = ''
            distance = 0
            duration = 0
            calories = 0

            if data['type']:
                activity_type = data['type']
            if data['intensity']:
                intensity = data['intensity']
            if data['start_time']:
                start_time = data['start_time']
            if data['distance']:
                distance = Decimal(data['distance'])
            if data['duration']:
                duration = Decimal(data['duration'])
            if data['calories']:
                calories = Decimal(data['calories'])

            if activity_type or intensity or start_time or distance or duration or calories:
                try:
                    fitness, create = Fitness.objects.get_or_create(
                        user=user,
                        timestamp=timestamp,
                        utc_offset=utc_offset,
                        source=source,
                        source_name=source_name
                    )

                    fitness.activity_type = activity_type
                    fitness.intensity = intensity
                    fitness.start_time = start_time
                    fitness.distance = distance
                    fitness.duration = duration
                    fitness.calories = calories
                    fitness.last_updated = last_updated
                    fitness.save()

                    logger.debug("Created Fitness object: {0}".format(fitness))
                except MultipleObjectsReturned:
                    logger.debug("Query returned multiple objects.")
