#########################################################
## Exceptions used in track.utils.generate_series_data ##
#########################################################


class QuerySetHasNoModel(Exception):

    """
    When the variable query_set has no property named 'model'
    """
    pass


class FieldNameNotFoundInModel(Exception):

    """
    When the field_name variable passed does not exist
    as a property in the query_set.model
    """
    pass


class FieldTypeNotSupported(Exception):

    """
    When the variable type of field_name is netiher a DateField, TimeField
    nor a DateTimeField
    """
    pass


class GroupTypeNotSupported(Exception):

    """
    When the group_type passed is not supported
    """
    pass
