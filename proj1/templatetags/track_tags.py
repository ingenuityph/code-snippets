import settings
import dateutil
import datetime

from django import template
from django.contrib.auth.models import Group
from django.db.models import Q

from foods.models import FoodMetricList, FoodMetric, FoodIntake
from track.utils import (
    is_within_threshold,
    get_current_sleep_average,
    get_current_step_average,
    get_current_weight,
    get_current_calorie_intake_average
)
from track.validic import Validic

register = template.Library()


@register.filter
def show_workdesk(user):
    """
    Checks if a user is a writer, editor, or a physician
    """
    groups = Group.objects.filter(Q(name='writer') | Q(name='editor') | Q(name='physician'))
    for group in user.groups.all():
        if group in groups:
            return True
    return False


@register.filter
def get_threshold_status(scale, value):
    """
    Returns 'outside' if the value is outside the scale threshold
    """
    if is_within_threshold(scale, value):
        return 'inside'
    return 'outside'


@register.filter
def get_sleep_average(patient, physician):
    """
    Returns current sleep average of patient in x days
    """
    return get_current_sleep_average(patient, physician)


@register.filter
def get_step_average(patient, physician):
    """
    Returns current step average of patient in x days
    """
    return get_current_step_average(patient, physician)


@register.filter
def get_weight(patient):
    """
    Returns current weight of patient
    """
    return get_current_weight(patient)


@register.filter
def get_calorie_intake_average(patient):
    """
    Returns current calorie intake of patient
    """
    return get_current_calorie_intake_average(patient)


@register.filter
def get_marketplace_url(user):
    """
    Returns the user's specific Validic Marketplace url
    """
    validic = Validic(
        settings.SB_ORGANIZATION_APP_ID,
        settings.SB_ORGANIZATION_SECRET,
        settings.SB_ORGANIZATION_ACCESS_TOKEN
    )

    validic_profile = validic.get_validic_profile(user)

    url = 'https://app.validic.com/%s/%s' % (
        settings.SB_ORGANIZATION_APP_ID,
        validic_profile.access_token
    )

    return url


@register.filter
def get_calories(food_intake_id):
    """
    Returns the calories of a food
    """
    food_intake = FoodIntake.objects.get(id=food_intake_id)
    food_metric = FoodMetric.objects.get(name='Calories')
    food_metriclist = FoodMetricList.objects.get(food=food_intake.food, metric=food_metric)

    calories = food_intake.serving * food_metriclist.value

    return calories


@register.filter
def next_week(date):
    """
    Returns the start date of next week
    """
    result_date = dateutil.parser.parse(str(date)).date() + datetime.timedelta(days=7)

    return result_date


@register.filter
def prev_week(date):
    """
    Returns the start date of prev week
    """
    result_date = dateutil.parser.parse(str(date)).date() + datetime.timedelta(days=-7)

    return result_date
