from django import forms
from django.contrib.auth.models import User
from django.conf import settings
from django.template.loader import render_to_string
from django.template import Context, Template
from django.contrib.sites.models import Site

from annoying.functions import get_object_or_None
from mailer import send_html_mail
from track.models import Sleep
from accounts.models import UserProfile, PhysicianSubscriptions
from articles.models import EmailContent
from track.utils import generate_username


class SleepForm(forms.ModelForm):

    class Meta:
        model = Sleep
        exclude = ('user', 'awaken', 'fell_asleep_in')


class AddPatientForm(forms.Form):
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        self.physician = kwargs.pop('physician', None)
        super(AddPatientForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        email = self.cleaned_data['email']
        if PhysicianSubscriptions.objects.filter(physician=self.physician, patient__email=email).count():
            raise forms.ValidationError("User is already subscribed.")

        return email

    def save(self):
        username = generate_username()
        password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(
            email=self.cleaned_data['email'],
            defaults={
                'username': username,
                'is_active': True,
            })
        if created:
            user.set_password(password)
            user.save()
        PhysicianSubscriptions.objects.create(physician=self.physician,
                                              patient=user,
                                              amount=0,
                                              payment_method='W',
                                              payment_status='W')
        waived_type = 'waived-temp' if created else 'waived-user'
        error = None
        email = get_object_or_None(EmailContent, name="%s: %s" % ('telemed', waived_type))

        if email:
            html = render_to_string("articles/applications/email_content_template.html", {"email": email})
            t = Template(html)
            c = Context({"user": user, "password": password, "provider": self.physician, "url": Site.objects.get_current().domain})
            send_html_mail(email.subject, email.text, t.render(c), settings.DEFAULT_FROM_EMAIL, [user.email])
        else:
            error = "Missing e-mail. Please contact administrator."

        return error
