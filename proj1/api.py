import datetime
import math
import numbers
import time
from decimal import Decimal

from django.contrib.auth.models import User
from django.db import connection
from django.db.models import (
    Avg,
    Min,
    Max,
    Sum,
)
from django.db.models.query import EmptyQuerySet
from tastypie.authorization import DjangoAuthorization
from tastypie.exceptions import NotFound
from tastypie.resources import Resource

from nutritas.provider.authentication import OAuth20Authentication
from nutritas.track.models import (
    BloodPressure,
    BloodProfile,
    BodyFat,
    BodyMassIndex,
    BodyWater,
    BodyWeight,
    BoneMass,
    Calorie,
    Floor,
    LeanBodyMass,
    MuscleMass,
    Sleep,
    Steps,
    VisceralFatRating,
)


authorization = DjangoAuthorization()
authentication = OAuth20Authentication()


class TrackDataUnit(object):

    def __init__(self):
        self.frequency = 'daily'
        self.time_axis = {}  # x axis
        self.data_axis = {}  # y axis

    def set_frequency(self, frequency):
        frequency = frequency.lower()

        if frequency in ('daily', 'yearly', 'monthly', 'weekly'):
            self.frequency = frequency
        else:
            self.frequency = 'daily'

    def set_time(self, attr, data):
        if self.frequency in ('yearly', 'monthly', 'weekly'):
            value = data.get('year')
            value = '%s -- %s' % (value, data.get('month', data.get('week')))\
                    if self.frequency in ('monthly', 'weekly') else value
        else:
            value = data.get(attr)

        self.time_axis.update({
            attr: {
                'label': attr.replace('_', ' ').title(),
                'value': value,
            }
        })

    def add_data(self, label, attr, value):
        self.data_axis.update({
            attr: {
                'label': label,
                'value': value
            }
        })

    def get_axes(self):
        return (
            self.time_axis,  # x
            self.data_axis,  # y
        )

    def to_dict(self):
        result = {}

        for attr, attr_data in self.time_axis.iteritems():
            result[attr] = attr_data.get('value')

        for attr, attr_data in self.data_axis.iteritems():
            result[attr] = attr_data.get('value')

        return result


class TrackResource(Resource):

    class Meta:
        authentication = authentication
        authorization = authorization
        object_class = TrackDataUnit
        limit = None
        max_limit = None

        detail_allowed_methods = []
        list_allowed_methods = ['get']
        include_resource_uri = False
        include_absolute_uri = False

    TrackModel = None
    time_attr = 'date_checked'
    data_attrs = ()
    data_attrs_derived = ()

    def obj_get_list(self, bundle, user_id=None, **kwargs):
        self.check_user(user_id)

        request = bundle.request
        frequency,\
            limit    ,\
            before   ,\
            after = self.get_params(request)
        extra    ,\
            values   ,\
            order = self.get_frequency_args(frequency, self.time_attr)

        if frequency == 'daily':
            annotations = {}
        else:
            annotations = {attr: Aggregate(attr) for attr, label, Aggregate in self.data_attrs}

        queryset = self.build_queryset(self.TrackModel, {
            'user__id': user_id,
            '%s__range' % self.time_attr: (after, before)
        }, extra, values, annotations, order)
        timeprop = values[-1] if values else self.time_attr
        items = self.build_dataset(queryset, frequency)

        return items

    def dehydrate(self, bundle):
        bundle.data = bundle.obj.to_dict()
        return bundle

    def alter_list_data_to_serialize(self, request, data):
        if 'meta' in data:
            del data['meta']

        frequency, _, _, _ = self.get_params(request)
        data.update({
            'chart': self.build_chartdata(data.get('objects', []), frequency),
        })

        return data

    #--- Native Methods
    def check_user(self, user_id=None):
        try:
            if not user_id:
                raise User.DoesNotExist()

            user = User.objects.get(pk=user_id)
        except User.DoesNotExist, User.MultipleObjectsReturned:
            raise NotFound()

    def build_queryset(self, ModelType, where, extra, values, annotations, order):
        if not ModelType:
            return EmptyQuerySet()

        return ModelType.objects\
            .filter(**where)\
            .extra(**extra)\
            .values(*values)\
            .annotate(**annotations)\
            .order_by(*order)

    def build_dataset(self, results, frequency):
        dataset = []
        dataattrs = self.data_attrs + self.data_attrs_derived
        timeprop = self.get_timeattr(frequency)

        for result in results:
            item = TrackDataUnit()
            item.set_frequency(frequency)
            item.set_time(timeprop, result)

            for attr, label, Aggregate in dataattrs:
                item.add_data(label, attr, result.get(attr))

            dataset.append(item)

        return dataset

    def build_chartdata(self, bundles, frequency):
        """
        Returns a dictionary that represents the cart itself. This method primarily interacts with
        TrackDataUnit instances to get the label, and other stuff
        """
        data_attrs = self.data_attrs + self.data_attrs_derived
        time_attr = self.get_timeattr(frequency)
        chartx = {
            time_attr: {
                'interval': 1,
                'label': time_attr.replace('_', ' ').title(),
                'range': (None, None),
            }
        }
        charty = {
            attr: {
                'interval': 0,
                'label': label,
                'range': (None, None),
            } for attr, label, Aggregate in data_attrs
        }

        # Talk about loops yo
        for bundle in bundles:
            item = bundle.obj

            for item_axis, chart_axis in zip(item.get_axes(), (chartx, charty)):
                for attr, item_attr in item_axis.iteritems():
                    chart_attr = chart_axis.get(attr, {})

                    axis_min, axis_max = chart_attr.get('range', (None, None))
                    axis_val = item_attr.get('value')
                    axis_min = axis_val if axis_min is None or axis_min >= axis_val else axis_min
                    axis_max = axis_val if axis_max is None or axis_max <= axis_val else axis_max

                    chart_attr.update({
                        'range': (axis_min, axis_max),
                        'interval': self.get_interval(attr, axis_max),
                    })

        return {
            'frequency': frequency,
            'x-axis': [{
                'interval': attr_data.get('interval', 1),
                'prop_name': attr,
                'label': attr_data.get('label'),
                'range': attr_data.get('range'),
            } for attr, attr_data in chartx.iteritems()],
            'y-axis': [{
                'interval': attr_data.get('interval', 1),
                'prop_name': attr,
                'label': attr_data.get('label'),
                'range': attr_data.get('range')
            } for attr, attr_data in charty.iteritems()]
        }

    def get_frequency_args(self, frequency, dateattr='date_checked'):
        """
        Returns a tuple containing: (extra, values, order) parameters for their respective
        queryset methods
        """
        values = []
        select = {}

        if frequency in ('yearly', 'monthly', 'weekly'):
            select.update({'year': 'year(%s)' % dateattr})
            values.append('year')

        if frequency == 'monthly':
            select.update({'month': 'month(%s)' % dateattr})
            values.append('month')
        elif frequency == 'weekly':
            select.update({'week': 'week(%s)' % dateattr})
            values.append('week')

        return ({'select': select}, values, values[:])

    def get_params(self, request):
        """
        Returns a tuple containing: (frequency, limit, before and after timestamp) parameters
        """
        before = request.GET.get('before')
        after = request.GET.get('after')

        return (
            request.GET.get('frequency', 'daily'),
            request.GET.get('limit', 20),
            datetime.datetime.fromtimestamp(float(before) if before else time.time()),
            datetime.datetime.fromtimestamp(float(after) if after else time.time()),
        )

    def get_timeattr(self, frequency):
        if frequency == 'yearly':
            return 'year'
        elif frequency == 'monthly':
            return 'month'
        elif frequency == 'weekly':
            return 'week'
        else:
            return self.time_attr

    def get_interval(self, attr, value):
        if not isinstance(value, numbers.Number):
            return 1

        value = int(abs(value // 1))
        return 10 ** int(math.log10(value)) if value > 0 else 1


#--- Body Composition Resources
class BodyFatResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'body-fat'

    TrackModel = BodyFat
    data_attrs = (
        ('body_fat', 'Body Fat', Avg),
    )


class BodyWaterResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'body-water'

    TrackModel = BodyWater
    data_attrs = (
        ('body_water', 'Body Water', Avg),
    )


class BodyWeightResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'body-weight'

    TrackModel = BodyWeight
    time_attr = 'date'
    data_attrs = (
        ('weight', 'Body Weight', Avg),
    )


class BodyMassIndexResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'body-mass-index'

    TrackModel = BodyMassIndex
    data_attrs = (
        ('bmi', 'Body Mass Index', Avg),
        ('weight', 'Weight (lbs)', Avg),
        ('height_ft', 'Height (ft)', Avg),
        ('height_inch', 'Height (in)', Avg),
    )


class BoneMassResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'bone-mass'

    TrackModel = BoneMass
    time_attr = 'date'
    data_attrs = (
        ('bone_mass', 'Bone Mass', Avg),
    )


class LeanBodyMassResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'lean-body-mass'

    TrackModel = LeanBodyMass
    data_attrs = (
        ('lean_body_mass', 'Lean Body Mass', Avg),
    )


class MuscleMassResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'muscle-mass'

    TrackModel = MuscleMass
    data_attrs = (
        ('muscle_mass', 'Muscle Mass', Avg),
    )


class VisceralFatRatingResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'visceral-fat-rating'

    TrackModel = VisceralFatRating
    data_attrs = (
        ('rating', 'Fat Rating', Avg),
    )


#--- Blood Resources
class BloodPressureResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'blood-pressure'

    TrackModel = BloodPressure
    data_attrs = (
        ('systolic', 'Systolic', Avg),
        ('diastolic', 'Diastolic', Avg),
    )


class BloodProfileResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'blood-profile'

    TrackModel = BloodProfile
    data_attrs = (
        ('LDL', 'LDL', Avg),
        ('HDL', 'HDL', Avg),
        ('glucose', 'Glucose', Avg),
        ('rbc_count', 'RBC Count', Avg),
        ('wbc_count', 'WBC Count', Avg),
        ('platelet_count', 'Platelet Count', Avg),
    )


#--- Activity Resources
class CalorieResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'calorie'

    TrackModel = Calorie
    time_attr = 'date'
    data_attrs = (
        ('calories', 'Calories Burned', Avg),
    )


class FloorResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'floor'

    TrackModel = Floor
    time_attr = 'date'
    data_attrs = (
        ('floors', 'Floors', Avg),
    )


class SleepResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'sleep'

    # last item per tuple not used
    data_attrs = (
        ('start', 'Start', Avg),
        ('stop', 'End', Avg),
        ('fell_asleep_in', 'Slept within (minutes)', Avg),
    )
    # last item per tuple not used
    data_attrs_derived = (
        ('duration', 'Duration', Avg),
    )

    def obj_get_list(self, bundle, user_id=None, **kwargs):
        self.check_user(user_id)

        request = bundle.request
        frequency,\
            limit    ,\
            before   ,\
            after = self.get_params(request)

        queryset, timeprop = self.build_queryset({
            'date_checked': 'start',
        }, user_id, before, after, frequency)
        items = self.build_dataset(queryset, frequency)

        return items

    def build_queryset(self, extracols, user_id, before, after, frequency):
        Meta = Sleep._meta
        table = Meta.db_table
        fields = ['%s as `%s`' % (expr, column) for column, expr in extracols.iteritems()]
        groups = []
        timeprop = None

        # Compile fields
        if frequency == 'daily':
            timeprop = 'date_checked'
            groups = ['`%s`.`%s`' % (table, Meta.pk.column)]
            fields.extend(['`%s`.`%s`' % (table, field.column) for field in Meta.fields])
        else:
            groups = ['year']
            select = {
                'start': 'from_unixtime(avg(unix_timestamp(`%s`.`start`)))' % table,
                'stop': 'from_unixtime(avg(unix_timestamp(`%s`.`stop`)))' % table,
                'fell_asleep_in': 'avg(`%s`.`fell_asleep_in`)' % table,
                'year': 'year(`%s`.`start`)' % table,
            }

            if frequency == 'monthly':
                groups.append('month')
                select.update({'month': 'month(`%s`.`start`)' % table})

            if frequency == 'weekly':
                groups.append('week')
                select.update({'week': 'week(`%s`.`start`)' % table})

            timeprop = groups[-1]
            fields.extend(['%s as `%s`' % (expr, column) for column, expr in select.iteritems()])

        sqlcursor = connection.cursor()
        sqlquery  = """
            SELECT
                %s
            FROM `%s`
            WHERE (
                `%s`.`start` BETWEEN %%s AND %%s AND
                `%s`.`user_id` = %%s
            )
            GROUP BY %s
            ORDER BY `start` ASC
        """ % (
            ', '.join(fields),
            table,
            table,
            table,
            ', '.join(groups)
        )

        sqlcursor.execute(sqlquery, (
            after.strftime('%Y-%m-%d %H:%M:%S'),
            before.strftime('%Y-%m-%d %H:%M:%S'),
            user_id
        ))
        columns = [column[0] for column in sqlcursor.description]
        rows = []

        for result in sqlcursor.fetchall():
            row = dict(zip(columns, result))
            row['duration'] = self.get_duration(row.get('start'), row.get('stop'))

            rows.append(row)

        return rows, timeprop

    def get_duration(self, start, stop):
        return round((stop - start).total_seconds() / 3600, 2)


class StepsResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'step'

    TrackModel = Steps
    time_attr = 'date'
    data_attrs = (
        ('steps', 'Steps Taken', Avg),
    )


#--- Miscellaneous Resources
class SummaryResource(TrackResource):

    class Meta(TrackResource.Meta):
        resource_name = 'summary'
        object_class = dict

    def obj_get_list(self, bundle, user_id=None, **kwargs):
        self.check_user(user_id)

        request = bundle.request
        timestamp = request.GET.get('timestamp', time.time())
        timestamp = datetime.datetime.fromtimestamp(float(timestamp))

        return [{
            'summary': {
                'steps': self.get_summary_steps(user_id, timestamp),
                'floors': self.get_summary_floors(user_id, timestamp),
                'calories': self.get_summary_calories(user_id, timestamp),
                'weight': self.get_summary_weight(user_id, timestamp),
            }
        }]

    def dehydrate(self, bundle):
        return bundle

    def alter_list_data_to_serialize(self, request, data):
        objects = data.get('objects', [])
        bundle = objects[0] if objects else None
        summary = bundle.obj if bundle else {}

        return summary

    def get_summary(self, Model, dataattr, user_id, dateparam):
        result = {}

        # Total
        result.update(
            Model.objects
            .filter(**{
                    'user__id': user_id,
                    'date': dateparam,
                    })
            .aggregate(today=Sum(dataattr))
        )

        # Average and Super Total
        result.update(
            Model.objects
            .filter(user__id=user_id)
            .aggregate(**{
                'average': Avg(dataattr),
                'total': Sum(dataattr),
            })
        )

        return {
            'today': self.normalize(result.get('today') or 0),
            'average': self.normalize(result.get('average') or 0),
            'total': self.normalize(result.get('total') or 0),
        }

    def get_summary_steps(self, user_id, dateparam):
        return self.get_summary(Steps, 'steps', user_id, dateparam)

    def get_summary_floors(self, user_id, dateparam):
        return self.get_summary(Floor, 'floors', user_id, dateparam)

    def get_summary_calories(self, user_id, dateparam):
        return self.get_summary(Calorie, 'calories', user_id, dateparam)

    def get_summary_weight(self, user_id, dateparam):
        result = BodyWeight.objects.aggregate(
            minimum=Min('weight'),
            maximum=Max('weight'),
        )

        return {
            'minimum': self.normalize(result.get('minimum') or 0),
            'maximum': self.normalize(result.get('maximum') or 0),
            'changes': self.normalize(result.get('maximum', 0) - result.get('minimum', 0))
        }

    ########## Utilities ##########
    def normalize(self, value):
        return float(Decimal(value).quantize(Decimal('0.00')))


track_resources = (
    BodyMassIndexResource,
    BodyFatResource,
    BodyWaterResource,
    BodyWeightResource,
    BoneMassResource,
    LeanBodyMassResource,
    MuscleMassResource,
    VisceralFatRatingResource,
    BloodPressureResource,
    BloodProfileResource,
    CalorieResource,
    FloorResource,
    SleepResource,
    StepsResource,
    SummaryResource
)
