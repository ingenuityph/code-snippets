"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import datetime

from datetime import timedelta
from django.contrib.auth.models import Group, User
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import RequestFactory

from accounts.models import (
    PhysicianSubscriptions
)
from track import models, views


class SimpleTest(TestCase):

    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)


# View Tests
class TrackDataTest(TestCase):

    """
    Tests Track module:
        * phr page
        * vital signs methods
    """

    def setUp(self):
        """
        Creates RequestFactory to simulate requests

        Creates test user:
            username -- 'test'
            email -- 'test@test.com'
            password -- 'testpassword'
        """
        self.factory = RequestFactory()
        self.user = User.objects.create_user('test', 'test@test.com', 'testpassword')

    def set_request(self, url, user):
        request = self.factory.get(url)
        request.user = user

        return request

    def test_phr_page(self):
        """
        Tests status codes on phr page
        """
        # Assert status code for phr page
        request = self.set_request(reverse('my_track_profile'), self.user)
        response = views.profile(request)
        self.assertEqual(response.status_code, 200)


class PatientRosterTest(TestCase):

    """
    Tests Patient Roster module:
        * patient roster page
        * patient phr page
        * thresholds page
        * adding patients
        * subscription
    """

    def setUp(self):
        """
        Creates RequestFactory to simulate requests

        Creates test user:
            username -- 'test'
            email -- 'test@test.com'
            password -- 'testpassword'

        Creates test patient:
            username -- 'patient'
            email -- 'patient@test.com'
            password -- 'patientpassword'

        Creates test group:
            name -- 'telemed'
        """
        self.factory = RequestFactory()
        self.user = User.objects.create_user('test', 'test@test.com', 'testpassword')
        self.patient = User.objects.create_user('patient', 'patient@test.com', 'patientpassword')
        self.group = Group.objects.create(name='telemed')
        self.patient_with_data = User.objects.create_user('patient2', 'patient2@test.com', 'patient2password')
        create_track_test_data(self.patient_with_data)

    def set_request(self, url, user):
        request = self.factory.get(url)
        request.user = user

        return request

    def test_patient_roster_page(self):
        """
        Tests status codes on visiting patient roster page
        """
        # Assert status code for existing user in telemed group
        self.group.user_set.add(self.user)
        request = self.set_request(reverse('roster'), self.user)
        response = views.roster(request)
        self.assertEqual(response.status_code, 200)

        # Assert status code for existing user not in telemed group
        self.group.user_set.remove(self.user)
        response = views.roster(request)
        self.assertEqual(response.status_code, 403)

    def test_patient_phr_page(self):
        """
        Tests status codes on visiting patient phr page
        """
        # Assert status code for telemed provider visit on own patient's phr page
        # Patient has no data
        self.group.user_set.add(self.user)
        self.subscription = PhysicianSubscriptions.objects.get_or_create(
            physician=self.user,
            patient=self.patient,
            amount=0
        )
        request = self.set_request(reverse('user_track_profile', kwargs={'patient_id': self.patient.id}), self.user)
        response = views.profile(request, self.patient.id)
        self.assertEqual(response.status_code, 200)

        # Assert status code for telemed provider visit on own patient's phr page
        # Patient has data
        self.subscription = PhysicianSubscriptions.objects.get_or_create(
            physician=self.user,
            patient=self.patient_with_data,
            amount=0
        )
        request = self.set_request(reverse('user_track_profile', kwargs={'patient_id': self.patient_with_data.id}), self.user)
        response = views.profile(request, self.patient_with_data.id)
        self.assertEqual(response.status_code, 200)

        # Assert status code for telemed provider visit on an anonymous user's phr page
        PhysicianSubscriptions.objects.get(physician=self.user, patient=self.patient).delete()
        response = views.profile(request, self.patient.id)
        self.assertEqual(response.status_code, 403)


def create_track_test_data(user):
    user = User.objects.get(username=user.username)
    today = datetime.datetime.today().date()
    yesterday = today - timedelta(1)
    time = datetime.datetime.now().time()

    calories1 = 400
    calories2 = 500
    floors1 = 20
    floors2 = 25
    steps1 = 1500
    steps2 = 2000

    models.Calorie.objects.create(user=user, calories=calories1, date=today, time=time)
    models.Calorie.objects.create(user=user, calories=calories2, date=yesterday, time=time)
    models.Floor.objects.create(user=user, floors=floors1, date=today, time=time)
    models.Floor.objects.create(user=user, floors=floors2, date=yesterday, time=time)
    models.Steps.objects.create(user=user, steps=steps1, date=today, time=time)
    models.Steps.objects.create(user=user, steps=steps2, date=yesterday, time=time)
