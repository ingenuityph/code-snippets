from datetime import datetime, timedelta
from django.db import models
from django.db.models.query import QuerySet
from track.utils import monthdelta


class TrackQuerySet(QuerySet):

    def date_filter(self, field_name, range_type, end_date=datetime.now(), *args, **kwargs):
        """
        params:
        - range_type (string): either 'hourly', daily', 'weekly' or 'monthly'
        - field_name (string): name of DateField/DateTimeField
        - end_date (datetime): if provided, it would replace the end-range
        """
        if range_type == "hourly":
            kwargs = {
                '{0}'.format(field_name): end_date
            }
            return self.filter(**kwargs)

        elif range_type in ["daily", "weekly", "monthly"]:
            date_range = []

            if range_type == "daily":
                start_date = end_date - timedelta(days=7)
                end_date = end_date + timedelta(days=1)
                date_range = [start_date.date(), end_date.date()]

            elif range_type == "weekly":
                start_date = monthdelta(end_date, -2)
                end_date = end_date + timedelta(days=1)
                date_range = [start_date.date(), end_date.date()]

            elif range_type == "monthly":
                start_date = monthdelta(end_date, -11)
                end_date = end_date + timedelta(days=1)
                date_range = [start_date.date(), end_date.date()]
            kwargs = {
                '{0}__range'.format(field_name): date_range
            }

            return self.filter(**kwargs)
        else:
            return self


class TrackManager(models.Manager):

    def get_query_set(self):
        return TrackQuerySet(self.model)

    def __getattr__(self, name):
        return getattr(self.get_query_set(), name)
