from django.conf.urls.defaults import *
from project_core.views import ComingSoonView


urlpatterns = patterns(
    'track.views',
    url(r'^$', 'index', name="track_index"),

    # Misc
    url(r'^profile/devices$', 'track_settings', name="track_settings"),
    url(r'^patient-roster/$', 'roster', name="roster"),
    url(r'^patient-roster/subscription/$', 'roster_subscription', name="roster-subscription"),
    url(r'^patient-roster/cancel/$', 'roster_cancel', name="roster-cancel"),
    url(r'^scales/$', 'scales', name="scales"),
    url(r'^scales/(?P<patient_id>\d+)$', 'scales', name="user_scales"),
    url(r'^ajax-sleep/$', 'ajax_sleep', name="ajax-sleep"),
    url(r'^ajax-sleep-submit/$', 'ajax_sleep_submit', name="ajax-sleep-submit"),
    url(r'^add-patients/$', 'add_patients', name='add-patients'),
    url(r'^check-mailed/$', 'check_mailed', name='check-mailed'),
    url(r'^device-settings/$', 'device_settings', name='device_settings'),
    url(r'^remove-device-account/$', 'remove_device_account', name='remove_device_account'),

    # food nutrition
    url(r'^profile/food-chart$', 'food_chart', name="food_chart"),

    # PHR for user
    url(r'^profile/$', 'profile', name="my_track_profile"),
    url(r'^profile/visceral-fat-rating$', 'visceral_fat', name="visceral_fat"),
    url(r'^profile/blood-pressure$', 'blood_pressure', name="blood_pressure"),
    url(r'^profile/body-mass-index$', 'body_mass_index', name="body_mass_index"),
    url(r'^profile/body-water$', 'body_water', name="body_water"),
    url(r'^profile/body-weight$', 'body_weight', name="body_weight"),
    url(r'^profile/bone-mass$', 'bone_mass', name="bone_mass"),
    url(r'^profile/muscle-mass$', 'muscle_mass', name="muscle_mass"),
    url(r'^profile/body-fat$', 'body_fat', name="body_fat"),
    url(r'^profile/lean-body-mass$', 'lean_body_mass', name="lean_body_mass"),
    url(r'^profile/blood-test$', 'blood_test', name="blood_test"),
    url(r'^profile/sleep$', 'ajax_sleep', name="sleep"),
    url(r'^profile/steps$', 'steps', name="steps"),
    url(r'^profile/floor$', 'floor', name="floor"),
    url(r'^profile/calorie$', 'calorie', name="calorie"),

    # PHR for patient
    url(r'^profile/(?P<patient_id>\d+)$', 'profile', name="user_track_profile"),
    url(r'^profile/visceral-fat-rating/(?P<patient_id>\d+)$', 'visceral_fat', name="patient_visceral_fat"),
    url(r'^profile/blood-pressure/(?P<patient_id>\d+)$', 'blood_pressure', name="patient_blood_pressure"),
    url(r'^profile/body-mass-index/(?P<patient_id>\d+)$', 'body_mass_index', name="patient_body_mass_index"),
    url(r'^profile/body-water/(?P<patient_id>\d+)$', 'body_water', name="patient_body_water"),
    url(r'^profile/body-weight/(?P<patient_id>\d+)$', 'body_weight', name="patient_body_weight"),
    url(r'^profile/bone-mass/(?P<patient_id>\d+)$', 'bone_mass', name="patient_bone_mass"),
    url(r'^profile/muscle-mass/(?P<patient_id>\d+)$', 'muscle_mass', name="patient_muscle_mass"),
    url(r'^profile/body-fat/(?P<patient_id>\d+)$', 'body_fat', name="patient_body_fat"),
    url(r'^profile/lean-body-mass/(?P<patient_id>\d+)$', 'lean_body_mass', name="patient_lean_body_mass"),
    url(r'^profile/blood-test/(?P<patient_id>\d+)$', 'blood_test', name="patient_blood_test"),
    url(r'^profile/sleep/(?P<patient_id>\d+)$', 'ajax_sleep', name="patient_sleep"),
    url(r'^profile/steps/(?P<patient_id>\d+)$', 'steps', name="patient_steps"),
    url(r'^profile/floor/(?P<patient_id>\d+)$', 'floor', name="patient_floor"),
    url(r'^profile/calorie/(?P<patient_id>\d+)$', 'calorie', name="patient_calorie"),

    # Ajax calls for user
    url(r'^profile/ajax/blood-glucose$', 'blood_glucose_ajax', name="blood_glucose_ajax"),
    url(r'^profile/ajax/blood-glucose-fasting-glucose$', 'blood_glucose_fasting_glucose_ajax', name="blood_glucose_fasting_glucose_ajax"),
    url(r'^profile/ajax/blood-glucose-oral-glucose$', 'blood_glucose_oral_glucose_ajax', name="blood_glucose_oral_glucose_ajax"),
    url(r'^profile/ajax/blood-glucose-random-glucose$', 'blood_glucose_random_glucose_ajax', name="blood_glucose_random_glucose_ajax"),
    url(r'^profile/ajax/blood-pressure$', 'blood_pressure_ajax', name="blood_pressure_ajax"),
    url(r'^profile/ajax/fitness-calories$', 'get_fitness_calories', name="get_fitness_calories"),

    # Ajax calls for patient
    url(r'^profile/ajax/blood-glucose/(?P<patient_id>\d+)$', 'blood_glucose_ajax', name="patient_blood_glucose_ajax"),
    url(r'^profile/ajax/blood-glucose-fasting-glucose/(?P<patient_id>\d+)$', 'blood_glucose_fasting_glucose_ajax', name="patient_blood_glucose_fasting_glucose_ajax"),
    url(r'^profile/ajax/blood-glucose-oral-glucose/(?P<patient_id>\d+)$', 'blood_glucose_oral_glucose_ajax', name="patient_blood_glucose_oral_glucose_ajax"),
    url(r'^profile/ajax/blood-glucose-random-glucose/(?P<patient_id>\d+)$', 'blood_glucose_random_glucose_ajax', name="patient_blood_glucose_random_glucose_ajax"),
    url(r'^profile/ajax/blood-pressure/(?P<patient_id>\d+)$', 'blood_pressure_ajax', name="patient_blood_pressure_ajax"),
    url(r'^profile/ajax/fitness-calories/(?P<patient_id>\d+)$', 'get_fitness_calories', name="patient_get_fitness_calories"),

    # 23andme api
    url(r'^23-and-me/callback$', 'callback_genetics', name="callback_genetics"),
    url(r'^profile/devices/unsync/23andme$', 'unsync_ttam_device', name="unsync_ttam_device"),

    # COMING SOON
    url(r'^coming-soon$', ComingSoonView.as_view(source="track", success_url_name="track_coming_soon"), name='track_coming_soon'),
)
