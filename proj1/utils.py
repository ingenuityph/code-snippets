import datetime
import logging
import pytz
import settings
import time as time_module

from django.contrib.auth.models import User
from django.core.exceptions import MultipleObjectsReturned
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db.models.fields import DateField, DateTimeField, TimeField
from django.template import Context, Template
from itertools import groupby
from notifications import notify
from notifications.models import Notification

from foods.models import Food, FoodMetricGroup, FoodMetricList, FoodMetric, FoodIntake
from nutritas.settings import EMAIL_FROM, THRESHOLD_EMAIL_PREFIX
from nutritas.track.exceptions import QuerySetHasNoModel, FieldNameNotFoundInModel, FieldTypeNotSupported, GroupTypeNotSupported

logger = logging.getLogger(__name__)


def monthdelta(date, delta):
    """
    Handles addition/subtraction of datetime
    params:
    date (date/datetime) - date/datetime type object
    delta (int) - integer represantation of months which
                  will be added when positive and deducted
                  when negative
    """
    m, y = (date.month + delta) % 12, date.year + ((date.month) + delta - 1) // 12
    if not m:
        m = 12
    d = min(date.day, [31, 29 if y % 4 == 0 and not y % 400 == 0 else 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1])
    return date.replace(day=d, month=m, year=y)


def generate_series_data(query_set, date_field, group_type, field_name, annotation_type="sum", formatted=False):
    """
    Handles grouping of queryset into days/week/month/year accordingly
    which will be used as date_display for series_data
    params:
    query_set (QuerySet) -the resulting query_set of model
    date_field (string) - date/datetime field which will be used as
                          a group condition for the itertools.groupby method
    group_type (string) - type of custon grouping
    annotation_type (string) - whether it will be grouped then summed/averaged
    """
    if not query_set.model:
        # raise QuerySetHasNoModel("Resulting variable query_set has no model. I have no idea why. %s" % query_set)
        logger.warning("[TRACK - generate_series_data()] Resulting variable query_set has no model. I have no idea why. %s" % query_set)
        return []

    model = query_set.model
    model_field_names = [field.name for field in model._meta.fields]

    if date_field not in model_field_names:
        # raise FieldNameNotFoundInModel("Resulting variable field name (%s) provided as date_field does not exist in model (%s)." % (date_field, model))
        logger.warning("[TRACK - generate_series_data()] Resulting variable field name (%s) provided as date_field does not exist in model (%s)." % (date_field, model))
        return []

    model_field = model._meta.get_field(date_field)

    # get_field_type
    field_type = "datetime" if type(model_field) == DateTimeField \
        else ("date" if type(model_field) == DateField
              else ("time" if type(model_field) == TimeField
                    else None))

    if not field_type:
        # raise FieldTypeNotSupported("Resulting variable field (%s) type (%s) is not supported for grouping." % (model_field, type(model_field)))
        logger.warning("[TRACK - generate_series_data()] Resulting variable field (%s) type (%s) is not supported for grouping." % (model_field, type(model_field)))
        return []

    group_methods = {
        "hourly": lambda record: getattr(record, date_field).hour,
        "daily": lambda record: getattr(record, date_field).day,
        "weekly": lambda record: '%s-%s' % (getattr(record, date_field).isocalendar()[1], getattr(record, date_field).year),
        "monthly": lambda record: getattr(record, date_field).month,
        "yearly": lambda record: getattr(record, date_field).year
    }

    if not group_type in group_methods:
        # raise GroupTypeNotSupported("Resulting variable group_type (%s) is not in list of custom grouping." % group_type)
        logger.warning("[TRACK - generate_series_data()] Resulting variable group_type (%s) is not in list of custom grouping." % group_type)
        return []

    series_data = []
    try:  # WIP, work on views to give proper field when using hour, since DateField does not have hour property.
        group_method = group_methods[group_type]

        grouper = groupby(query_set, group_method)

        for key, group in grouper:  # int, generator object
            # initialize
            aggregation = 0
            count = 0
            series_key = None

            # compile accordingly
            if formatted:
                week = ("1st", "2nd", "3rd", "4th", "5th")
                date_str = "%a, %d %b '%y"
                if group_type == 'monthly':
                    date_str = "%B %Y"
                elif group_type == 'yearly':
                    date_str = "%Y"

                for element in group:
                    if group_type == 'weekly':
                        element_date = getattr(element, date_field)
                        week_number = (element_date.day - 1) // 7 + 1
                        series_key = "%s week of %s" % (week[week_number - 1], element_date.strftime("%b '%y"))
                    else:
                        series_key = getattr(element, date_field).strftime(date_str)
                    aggregation += getattr(element, field_name)
                    count += 1
            else:
                for element in group:
                    series_key = time_module.mktime(getattr(element, date_field).timetuple()) * 1000
                    aggregation += getattr(element, field_name)
                    count += 1

            # convert summation into an average
            if annotation_type == "average" and count:
                aggregation /= count

            series_data.append([series_key, round(aggregation, 2)])
    except Exception, e:
        logger.error(e)

    return series_data


def calculate_bmi(height_ft=0, height_inch=0, weight=0):
    """
    method to calculate bmi
    """

    kg_weight = weight * 0.45
    m_height = ((height_ft * 12) + height_inch) * 0.025

    body_volume = m_height * m_height

    bmi = kg_weight / body_volume

    return bmi

"""
def calculate_lean_mass(male, weight, height):
    if male:
        #Lean Body Weight (men) = (1.10 x Weight(kg)) - 128 x ( Weight2/(100 x Height(m))2)
        height_2 = (100 * height) * (100 * height)
        lean_body_weight = (1.10 * weight) - 128 * ((weight*weight) / height_2)
    else:
    #Lean Body Weight (women) = (1.07 x Weight(kg)) - 148 x ( Weight2/(100 x Height(m))2)

"""


def simple_chart_data(categories="", values="", title="", series_name=""):
    """
    creates a json string for highcharts.
    Uses only 1 axis. improve later
    """

    chart_data = "{\
                    chart: {\
                    },\
                    title: {text: '" + title + "'},\
                    xAxis: {\
                        categories: [" + categories + "]\
                    },\
                    yAxis:{\
                       plotBands:[{color:'green', from: '20', to: '3',}], \
                    },\
                    \
                    series: [{\
                        name: '" + series_name + "',                      \
                        data: [" + values + "]        \
                    }]}"

    return chart_data


def blood_glucose_chart_data(categories, insulin=""):
    """
    Chart to be used for Insulin
    """
    chart_data = {
        "chart": {
            "type": "spline"
        },
        "title": {"text": "INSULIN"},
        "xAxis": {"categories": categories},
        "yAxis": [{
            "title": {
                "text": "Insulin (pmol/L)"
            },
            "labels": {
                "style": {
                    "fontWeight": "bold",
                    "color": "gray"
                }
            }
        }
        ],
        "tooltip": {
            "shared": True
        },
        "legend": {
            "layout": "vertical",
            "align": "left",
            "x": 120,
            "verticalAlign": "top",
            "y": 20,
            "floating": True,
            "backgroundColor": "white",
        },
        "series": [{
            "name": "Insulin",
            "data": insulin["data"]
        }
        ]
    }
    return chart_data


def blood_glucose_fasting_glucose_chart_data(categories, fasting_plasma=""):
    """
    Chart to be used for Fasting Plasma
    """
    chart_data = {
        "chart": {
            "type": "spline"
        },
        "title": {"text": "FASTING PLASMA GLUCOSE"},
        "xAxis": {"categories": categories},
        "yAxis": [{
            "title": {
                "text": "Fasting Plasma (mmol/L)"
            },
            "labels": {
                "style": {
                    "fontWeight": "bold",
                    "color": "gray"
                }
            }
        }
        ],
        "tooltip": {
            "shared": True
        },
        "legend": {
            "layout": "vertical",
            "align": "left",
            "x": 120,
            "verticalAlign": "top",
            "y": 20,
            "floating": True,
            "backgroundColor": "white",
        },
        "series": [{
            "name": "Fasting Plasma",
            "data": fasting_plasma["data"]
        }
        ]
    }
    return chart_data


def blood_glucose_oral_glucose_chart_data(categories, oral_glucose=""):
    """
    Chart to be used for Oral Glucose Tolerance Test
    """
    chart_data = {
        "chart": {
            "type": "spline"
        },
        "title": {"text": "ORAL GLUCOSE TOLERANCE"},
        "xAxis": {"categories": categories},
        "yAxis": [{
            "title": {
                "text": "Oral Glucose Tolerance (mmol/L)"
            },
            "labels": {
                "style": {
                    "fontWeight": "bold",
                    "color": "gray"
                }
            }
        }
        ],
        "tooltip": {
            "shared": True
        },
        "legend": {
            "layout": "vertical",
            "align": "left",
            "x": 120,
            "verticalAlign": "top",
            "y": 20,
            "floating": True,
            "backgroundColor": "white",
        },
        "series": [{
            "name": "Oral Glucose Tolerance",
            "data": oral_glucose["data"]
        }
        ]
    }
    return chart_data


def blood_glucose_random_glucose_chart_data(categories, random_glucose=""):
    """
    Chart to be used for Random Plasma Glucose Test
    """
    chart_data = {
        "chart": {
            "type": "spline"
        },
        "title": {"text": "RANDOM PLASMA GLUCOSE"},
        "xAxis": {"categories": categories},
        "yAxis": [{
            "title": {
                "text": "Random Plasma Glucose (mmol/L)"
            },
            "labels": {
                "style": {
                    "fontWeight": "bold",
                    "color": "gray"
                }
            }
        }
        ],
        "tooltip": {
            "shared": True
        },
        "legend": {
            "layout": "vertical",
            "align": "left",
            "x": 120,
            "verticalAlign": "top",
            "y": 20,
            "floating": True,
            "backgroundColor": "white",
        },
        "series": [{
            "name": "Random Plasma Glucose",
            "data": random_glucose["data"]
        }
        ]
    }
    return chart_data


def blood_pressure_chart_data(categories, systolic="", diastolic=""):
    """
    returns a chart to be used for blood pressure data
    """
    chart_data = {
        "chart": {
            "type": "column"
        },
        "title": {"text": "Blood Pressure"},
        "xAxis": {"categories": categories},
        "yAxis": {
            "title": {
                "text": "Systolic / Diastolic (Hg)"
            },
            "labels": {
                "style": {
                    "fontWeight": "bold",
                    "color": "gray"
                }
            }
        },

        "plotOptions": {
            "column": {
                "pointPadding": 0.2,
                "borderWidth": 0
            }
        },

        "series": [
            {
                "name": "Systolic",
                "data": systolic["data"]
            },
            {
                "name": "Diastolic",
                "data": diastolic["data"]
            }
        ],

        "tooltip": {
            "shared": True
        },

    }

    return chart_data


def update_scale_min_max(scale, value):
    """
    Updates minimum and maximum values of scale
    """
    scale.min_value = value[0]
    scale.max_value = value[1]
    scale.save()


def calculate_scale_default(scale):
    """
    Calculates the default for minimum and maximum values
    """
    if scale.min_value == 0 and scale.max_value == 0:
        scale.calculate_default()


def is_within_threshold(scale, value):
    """
    Returns true if the value is within scale threshold
    """
    if value >= scale.min_value and value <= scale.max_value:
        return True

    return False


def get_current_sleep_average(patient, physician):
    """
    Returns the current sleep average of the patient in x days
    """
    from track.models import Sleep, SleepScale

    try:
        days = SleepScale.objects.get(user=physician, patient=patient).time_component
    except SleepScale.DoesNotExist:
        days = 1

    today = datetime.datetime.today()
    start_date = today - datetime.timedelta(days=days - 1)

    sleeps = Sleep.objects.filter(user=patient, stop__gte=start_date, stop__lte=today)

    sleep_sum = 0
    for sleep in sleeps:
        sleep_sum += sleep.hours

    return round(sleep_sum / days, 2)


def get_current_step_average(patient, physician):
    """
    Returns the current step average of the patient in x days
    """
    from track.models import Steps, StepScale

    try:
        days = StepScale.objects.get(user=physician, patient=patient).time_component
    except StepScale.DoesNotExist:
        days = 1

    today = datetime.datetime.today().date()
    start_date = today - datetime.timedelta(days=days - 1)

    steps = Steps.objects.filter(user=patient, date__gte=start_date, date__lte=today)

    steps_sum = 0
    for step in steps:
        steps_sum += step.steps

    return round(steps_sum / days, 2)


def get_current_weight(patient):
    """
    Returns the current weight of the patient
    """
    from track.models import BodyWeight

    weights = BodyWeight.objects.filter(user=patient)

    return weights[0].weight if weights.count() > 0 else 0


def get_current_systolic_bp(patient):
    """
    Returns the current systolic blood pressure of the patient
    """
    from track.models import BloodPressure

    blood_pressure = BloodPressure.objects.filter(user=patient)

    return blood_pressure[0].systolic if blood_pressure.count() > 0 else 0


def get_current_diastolic_bp(patient):
    """
    Returns the current diastolic blood pressure of the patient
    """
    from track.models import BloodPressure

    blood_pressure = BloodPressure.objects.filter(user=patient)

    return blood_pressure[0].diastolic if blood_pressure.count() > 0 else 0


def get_current_blood_glucose(patient):
    """
    Returns the current blood glucose(mmol/L) of the patient
    """
    from track.models import BloodProfile

    blood_profile = BloodProfile.objects.filter(user=patient)

    return blood_profile[0].glucose if blood_profile.count() > 0 else 0


def get_current_calories_burned(patient):
    """
    Returns the current calories burned(cal) of the patient
    """
    from track.models import Calorie

    calories = Calorie.objects.filter(user=patient)

    return calories[0].calories if calories.count() > 0 else 0


def get_current_calorie_intake_average(patient):
    """
    Returns the current calorie intake average(cal) of the patient
    """
    from accounts.models import UserProfile
    from foods.models import FoodMetricList, FoodMetric, FoodIntake

    days = 7
    calorie_intake_average = 0
    food_intake_list = None
    today = datetime.datetime.today().date()
    start_date = today - datetime.timedelta(days=days - 1)

    try:
        patient_userprofile, is_created = UserProfile.objects.get_or_create(user=patient)
    except MultipleObjectsReturned:
        patient_userprofile = None

    if patient_userprofile:
        food_intake_list = FoodIntake.objects.filter(
            userprofile=patient_userprofile,
            datetime_taken__gte=start_date,
            datetime_taken__lte=today
        )

    try:
        food_metric, create = FoodMetric.objects.get_or_create(name='Calories')
    except MultipleObjectsReturned:
        food_metric = FoodMetric.objects.filter(name='Calories')[0]

    total_calorie_intake = 0
    for food_intake in food_intake_list:
        try:
            food_metriclist = FoodMetricList.objects.get(
                food=food_intake.food,
                metric=food_metric
            )
            total_calorie_intake += food_intake.serving * food_metriclist.value
        except FoodMetricList.DoesNotExist:
            pass
    calorie_intake_average = total_calorie_intake / days

    return calorie_intake_average


def check_if_within_threshold(patient, physician):
    """
    Sends notifications if patient data exceeds notification threshold
    """
    from track.models import (
        BloodGlucoseScale,
        BodyWeightScale,
        CaloriesScale,
        DiastolicBloodPressureScale,
        SleepScale,
        StepScale,
        SystolicBloodPressureScale,
        CalorieIntakeScale
    )

    data = (
        ('weight', BodyWeightScale, get_current_weight(patient)),
        ('sleep', SleepScale, get_current_sleep_average(patient, physician)),
        ('step', StepScale, get_current_step_average(patient, physician)),
        ('systolic blood pressure', SystolicBloodPressureScale, get_current_systolic_bp(patient)),
        ('diastolic blood pressure', DiastolicBloodPressureScale, get_current_diastolic_bp(patient)),
        ('blood glucose', BloodGlucoseScale, get_current_blood_glucose(patient)),
        ('calories burned', CaloriesScale, get_current_calories_burned(patient)),
        ('calorie intake', CalorieIntakeScale, get_current_calorie_intake_average(patient))
    )

    for x in data:
        try:
            scale_instance, create = x[1].objects.get_or_create(
                user=physician,
                patient=patient
            )
            if create:
                calculate_scale_default(scale_instance)
        except MultipleObjectsReturned:
            scale_instance = None

        if scale_instance:
            if not is_within_threshold(scale_instance, x[2]):
                url = reverse('user_scales', kwargs={'patient_id': patient.id})
                prev_notifs = Notification.objects.filter(
                    actor_object_id=patient.id,
                    recipient=physician,
                    verb=u'has exceeded the %s scale threshold' % x[0],
                    unread=True
                )
                # Check if same notifications are still unread
                if not prev_notifs:
                    # Facebook like notifications for physician if patient data exceeds threshold
                    notify.send(
                        patient,
                        recipient=physician,
                        verb=u'has exceeded the %s scale threshold' % x[0],
                        target=None,
                        url=url
                    )

                    # Email notifications for physicians if patient data exceeds thresholds
                    email_content = '%s has exceeded the %s scale threshold' % (patient, x[0])
                    template = Template(email_content).render(Context({}))
                    try:
                        send_mail(
                            '%s Notification Threshold' % THRESHOLD_EMAIL_PREFIX,
                            template,
                            EMAIL_FROM,
                            [physician.email],
                            fail_silently=False
                        )
                    except Exception as e:
                        logger.warn("Threshold notification sending failed.  %s", e.message)


def get_sync_log(user, action):
    """
    Gets sync log or none
    """
    from track.models import SyncLog

    try:
        sync_log = SyncLog.objects.get(user=user, action=action)
    except SyncLog.DoesNotExist:
        sync_log = None

    return sync_log


def pull_data_for_validic(user):
    """
    Pulls all data(fitness, routine, nutrition, sleep, weight, diabetes measurements,
    biometric measurements, tobacco cessation) for a Validic User

    Currently gets data(biometrics, routine, weight, sleep) with timestamps within
    yesterday

    All dates used(start_date, timestamp, etc...) uses ISO 8601 standard format

    TODO:
    * Should get timestamp from last pull of data
    * Do query from last pull of data to present by adding start_date argument
    to get method with the desired timestamp
    """
    from track.validic import Validic

    validic = Validic(
        settings.SB_ORGANIZATION_APP_ID,
        settings.SB_ORGANIZATION_SECRET,
        settings.SB_ORGANIZATION_ACCESS_TOKEN
    )

    biometrics_sync_log = get_sync_log(user, 'biometrics')
    routine_sync_log = get_sync_log(user, 'routine')
    weight_sync_log = get_sync_log(user, 'weight')
    sleep_sync_log = get_sync_log(user, 'sleep')
    fitness_sync_log = get_sync_log(user, 'fitness')

    try:
        if biometrics_sync_log:
            validic.set_biometrics(user, validic.get_biometrics(user, start_date=biometrics_sync_log.timestamp)['biometrics'])
        else:
            validic.set_biometrics(user, validic.get_biometrics(user, start_date='2013-09-01T00:00:00+00:00')['biometrics'])
    except KeyError:
        pass

    try:
        if routine_sync_log:
            validic.set_routine(user, validic.get_routine(user, start_date=routine_sync_log.timestamp)['routine'])
        else:
            validic.set_routine(user, validic.get_routine(user, start_date='2013-09-01T00:00:00+00:00')['routine'])
    except KeyError:
        pass

    try:
        if weight_sync_log:
            validic.set_weight(user, validic.get_weight(user, start_date=weight_sync_log.timestamp)['weight'])
        else:
            validic.set_weight(user, validic.get_weight(user, start_date='2013-09-01T00:00:00+00:00')['weight'])
    except KeyError:
        pass

    try:
        if sleep_sync_log:
            validic.set_sleep(user, validic.get_sleep(user, start_date=sleep_sync_log.timestamp)['sleep'])
        else:
            validic.set_sleep(user, validic.get_sleep(user, start_date='2013-09-01T00:00:00+00:00')['sleep'])
    except KeyError:
        pass

    try:
        if fitness_sync_log:
            validic.set_fitness(user, validic.get_fitness(user, start_date=fitness_sync_log.timestamp)['fitness'])
        else:
            validic.set_fitness(user, validic.get_fitness(user, start_date='2013-09-01T00:00:00+00:00')['fitness'])
    except KeyError:
        pass


def fitness_calories_chart_data(timeline, running_series, swimming_series, jogging_series, other_activities_series):
    """
    returns a chart to be used for fitness data
    """
    chart_data = {
        "chart": {
            "type": "column"
        },
        "title": {"text": "Fitness Calories"},
        "xAxis": {"categories": timeline},
        "yAxis": {
            "title": {
                "text": "Steps"
            },
            "labels": {
                "style": {
                    "fontWeight": "bold",
                    "color": "gray"
                }
            }
        },

        "plotOptions": {
            "column": {
                "pointPadding": 0.2,
                "borderWidth": 0
            }
        },

        "series": [
            {
                "name": "Running",
                "data": running_series["data"]
            },
            {
                "name": "Swimming",
                "data": swimming_series["data"]
            },
            {
                "name": "Jogging",
                "data": jogging_series["data"]
            },
            {
                "name": "Others",
                "data": other_activities_series["data"]
            }
        ],

        "tooltip": {
            "shared": True
        },

    }

    return chart_data


def convert_weight(value, label):
    """
    Converts weight in kilos to pounds, or vice versa
    """
    multiplier = 2.2
    if label == 'kgs':
        value = value * multiplier
    else:
        value = value / multiplier

    return value


def generate_username():
    """
    Generates random username for automatically created users
    """
    rand_str = User.objects.make_random_password(length=7)
    username = "user-%s" % rand_str

    try:
        User.objects.get(username=username)
        return generate_username()
    except User.DoesNotExist:
        return username


# 23andMe Methods
def pull_data_for_23andme(user):
    """
    Write description
    """
    from accounts.models import GeneticProfile
    from track import client
    from track.models import HealthRisk, HealthCarrier, HealthDrugResponse, HealthTrait

    c = client.OAuthClient()

    if 'code' in request.GET:
        code = request.GET["code"]
        logger.debug("code: %s" % code)

        logger.debug("fetching token...")

        (access_token, refresh_token) = c.get_token(code)
        logger.debug("access_token: %s refresh_token: %s" % (access_token, refresh_token))

        logger.debug("refreshing token...")

        (access_token, refresh_token) = c.refresh_token(refresh_token)
        logger.debug("access_token: %s refresh_token: %s" % (access_token, refresh_token))

        request.session[client.OAUTH_KEY] = access_token

        c = client.OAuthClient(request.session[client.OAUTH_KEY])
        now = datetime.datetime.now()

        try:
            genetic_profile = GeneticProfile.objects.get(user=user)
        except GeneticProfile.DoesNotExist:
            genetic_profile = None

        if genetic_profile:
            risks_json = c.get_risks(genetic_profile.profile_id)
            carriers_json = c.get_carriers(genetic_profile.profile_id)
            drug_responses_json = c.get_drug_responses(genetic_profile.profile_id)
            traits_json = c.get_traits(genetic_profile.profile_id)

            try:
                for risk in risks_json['risks']:
                    health_trait, health_trait_created = HealthTrait.objects.get_or_create(
                        user=user,
                        timestamp=now
                    )
                    health_trait.description = risk['description']
                    health_trait.report_id = risk['report_id']
                    health_trait.population_risk = risk['population_risk']
                    health_trait.risk = risk['risk']
                    health_trait.save()
            except KeyError:
                pass

            try:
                if carrier in carriers_json['carriers']:
                    health_carrier, health_carrier_created = HealthCarrier.objects.get_or_create(
                        user=user,
                        timestamp=now
                    )
                    health_carrier.description = carrier['description']
                    health_carrier.report_id = carrier['report_id']
                    health_carrier.mutation = carrier['mutations']
                    health_carrier.save()
            except KeyError:
                pass

            try:
                if drug_response in drug_responses_json['drug_responses']:
                    health_drug_response, health_drug_response_created = HealthDrugResponse.objects.get_or_create(
                        user=user,
                        timestamp=now
                    )
                    health_drug_response.description = drug_response['description']
                    health_drug_response.report_id = drug_response['report_id']
                    health_drug_response.status = drug_response['status']
                    health_drug_response.save()
            except KeyError:
                pass

            try:
                if trait in traits_json['traits']:
                    health_trait, health_trait_created = HealthTrait.objects.get_or_create(
                        user=user,
                        timestamp=now
                    )
                    health_trait.description = trait['description']
                    health_trait.report_id = trait['report_id']
                    health_trait.trait = trait['trait']
                    health_trait.save()
            except KeyError:
                pass


def get_distinct_report_ids(class_instance, user):
    distinct_report_ids = class_instance.objects.filter(
        user=user
    ).values('report_id').distinct()

    return distinct_report_ids


def get_latest_object_with_report_id(class_instance, user, report_id):
    latest_object = class_instance.objects.filter(
        user=user,
        report_id=report_id
    ).order_by('-timestamp')[0]

    return latest_object


def update_ttam_list(ttam_list, obj_to_append, list_limit, other_checker=True):
    if other_checker and len(ttam_list) < list_limit:
        ttam_list.append(obj_to_append)
    elif other_checker and not list_limit:
        ttam_list.append(obj_to_append)

    return ttam_list


def get_ttam_health(user, return_lists, class_instance):
    """
    Utility method to get 23andMe health data

    user -- (User) the user the health data belongs to
    return_lists -- (list) a list of dictionaries with details regarding
        output of each health(risks, carriers, drug_responses, traits) query
    class_instance -- (class) the class from which to get the health data,
        i.e. HealthRisk, HealthCarrier, HealthDrugResponse, HealthTrait

    Returns a dictionary of health data, i.e.:
    health_dict = {
        'list_name[n]': n lists associated with the return_lists parameter,
        'total_count': count of all risks
    }
    """
    # Gets distinct report ids
    distinct_report_ids = get_distinct_report_ids(class_instance, user)

    other_checker = True
    for report_id in distinct_report_ids:
        # Gets latest health instance with report_id
        health_instance = get_latest_object_with_report_id(
            class_instance,
            user,
            report_id['report_id']
        )

        # Updates appropriate lists
        return_limit_reached = False
        for return_list in return_lists:
            if return_list['other_checker']:
                if return_list['other_checker'][1]:
                    other_checker = getattr(health_instance, return_list['other_checker'][0])
                else:
                    other_checker = not getattr(health_instance, return_list['other_checker'][0])

            return_list['return_list'] = update_ttam_list(
                return_list['return_list'],
                health_instance,
                return_list['return_limit'],
                other_checker
            )

            if len(return_list['return_list']) == return_list['return_limit']:
                return_limit_reached = True
            else:
                return_limit_reached = False

        if return_limit_reached:
            break

    health_dict = {
        'total_count': len(distinct_report_ids)
    }

    for return_list in return_lists:
        health_dict[return_list['list_name']] = return_list['return_list']

    return health_dict


def get_ttam_risks(user, elevated_count=None, reduced_count=None):
    """
    Gets 23andMe Disease Risks from HealthRisk model

    user -- (User) the user the disease risks belongs to
    elevated_count -- (int) the count of elevated risks to return by default,
        if None, returns all elevated risks
    reduced_count -- (int) the count of reduced risks to return by default,
        if None, returns all reduced risks

    Returns a dictionary of risks, i.e.:
    risks = {
        'elevated': list of all elevated risks,
        'reduced': list of all reduced risks,
        'total_count': count of all risks
    }
    """
    from track.models import HealthRisk

    return_lists = [
        {
            'return_list': [],
            'return_limit': elevated_count,
            'list_name': 'elevated',
            'other_checker': ('is_elevated', True)
        },
        {
            'return_list': [],
            'return_limit': reduced_count,
            'list_name': 'reduced',
            'other_checker': ('is_elevated', False)
        }
    ]

    risks = get_ttam_health(user, return_lists, HealthRisk)

    return risks


def get_ttam_carriers(user, carrier_count=None):
    """
    Gets 23andMe Carrier Status from HealthCarrier model

    user -- (User) the user the carrier status belongs to
    carrier_count -- (int) the count of carriers to return by default,
        if None, returns all carriers

    Returns a dictionary of carriers, i.e.:
    carriers = {
        'carriers': list of all carriers,
        'total_count': count of all carriers
    }
    """
    from track.models import HealthCarrier

    return_lists = [
        {
            'return_list': [],
            'return_limit': carrier_count,
            'list_name': 'carriers',
            'other_checker': None
        }
    ]

    carriers = get_ttam_health(user, return_lists, HealthCarrier)

    return carriers


def get_ttam_drug_responses(user, drug_response_count=None):
    """
    Gets 23andMe Drug Responses from HealthDrugResponse model

    user -- (User) the user the drug responses belongs to
    drug_response_count -- (int) the count of drug responses to return by default,
        if None, returns all drug responses

    Returns a dictionary of drug responses, i.e.:
    drug_responses = {
        'drug_responses': list of all drug responses,
        'total_count': count of all drug responses
    }
    """
    from track.models import HealthDrugResponse

    return_lists = [
        {
            'return_list': [],
            'return_limit': drug_response_count,
            'list_name': 'drug_responses',
            'other_checker': None
        }
    ]

    drug_responses = get_ttam_health(user, return_lists, HealthDrugResponse)

    return drug_responses


def get_ttam_traits(user, trait_count=None):
    """
    Gets 23andMe Traits from HealthTrait model

    user -- (User) the user the traits belongs to
    trait_count -- (int) the count of traits to return by default,
        if None, returns all traits

    Returns a dictionary of traits, i.e.:
    traits = {
        'traits': list of all traits,
        'total_count': count of all traits
    }
    """
    from track.models import HealthTrait

    return_lists = [
        {
            'return_list': [],
            'return_limit': trait_count,
            'list_name': 'traits',
            'other_checker': None
        }
    ]

    traits = get_ttam_health(user, return_lists, HealthTrait)

    return traits


def get_calories(food_intake_id):
    """
    Returns the calories of a food
    """
    food_intake = FoodIntake.objects.get(id=food_intake_id)
    food_metric = FoodMetric.objects.get(name='Calories')
    food_metriclist = FoodMetricList.objects.get(
        food=food_intake.food,
        metric=food_metric
    )

    calories = food_intake.serving * food_metriclist.value

    return calories


def get_total_calories(user, current_date, offset):
    """
    Returns the calories in a day
    """
    total_calories = 0

    local_time = datetime.datetime.strptime(str(current_date) + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
    gt_this = local_time - datetime.timedelta(hours=offset)
    lt_this = (local_time + datetime.timedelta(days=1, seconds=-1)) - datetime.timedelta(hours=offset)

    gt_this = gt_this.replace(tzinfo=pytz.UTC)
    lt_this = lt_this.replace(tzinfo=pytz.UTC)

    try:
        user_foodintake = FoodIntake.objects.filter(datetime_taken__range=(gt_this, lt_this), userprofile=user.userprofile)
        food_metric = FoodMetric.objects.get(name='Calories')
    except Exception as e:
        logger.warning(e)

    try:
        for food_intake in user_foodintake:
            food_metriclist = FoodMetricList.objects.get(
                food=food_intake.food,
                metric=food_metric
            )
            calories = food_intake.serving * food_metriclist.value
            total_calories += calories
    except Exception as e:
        logger.warning(e)

    return total_calories


def get_week_startdate(today=datetime.date.today(), timedelta_input=0):
    """
    Returns start date of week
    """
    weekday = today.weekday()
    start_delta = datetime.timedelta(days=weekday)
    start_of_currweek = (today - start_delta) + datetime.timedelta(int(timedelta_input))  # current week

    return start_of_currweek


def render_chart_data(title_text, series_name, categories, data):
    """
    returns a chart to be used for blood pressure data
    """
    chart_data = {
        "chart": {
            "type": "column"
        },
        "title": {"text": title_text},
        "xAxis": {"categories": categories},
        "yAxis": {
            "title": {
                "text": title_text
            },
            "labels": {
                "style": {
                    "fontWeight": "bold",
                    "color": "gray"
                }
            }
        },

        "plotOptions": {
            "column": {
                "pointPadding": 0.2,
                "borderWidth": 0
            }
        },

        "series": [
            {
                "name": series_name,
                "data": data
            },
        ],

        "tooltip": {
            "shared": True
        },

    }

    return chart_data
