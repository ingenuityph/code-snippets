# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'NewsArticle'
        db.create_table(u'news_newsarticle', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=1024)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('image', self.gf(u'main.db.fields.AutoResizeImageField')(max_length=100, null=True, blank=True)),
            ('published', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'news', ['NewsArticle'])

        # Adding model 'NewsArticleGenre'
        db.create_table(u'news_newsarticlegenre', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('article', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.NewsArticle'])),
            ('genre', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.Genre'])),
        ))
        db.send_create_signal(u'news', ['NewsArticleGenre'])

        # Adding unique constraint on 'NewsArticleGenre', fields ['article', 'genre']
        db.create_unique(u'news_newsarticlegenre', ['article_id', 'genre_id'])

        # Adding model 'NewsArticleKeyword'
        db.create_table(u'news_newsarticlekeyword', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('article', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.NewsArticle'])),
            ('keyword', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.Keyword'])),
        ))
        db.send_create_signal(u'news', ['NewsArticleKeyword'])

        # Adding unique constraint on 'NewsArticleKeyword', fields ['article', 'keyword']
        db.create_unique(u'news_newsarticlekeyword', ['article_id', 'keyword_id'])

        # Adding model 'NewsSource'
        db.create_table(u'news_newssource', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('type', self.gf('django.db.models.fields.SmallIntegerField')(default=1)),
            ('lastfetch', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'news', ['NewsSource'])

        # Adding model 'NewsSourceGenre'
        db.create_table(u'news_newssourcegenre', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.NewsSource'])),
            ('genre', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.Genre'])),
        ))
        db.send_create_signal(u'news', ['NewsSourceGenre'])

        # Adding unique constraint on 'NewsSourceGenre', fields ['source', 'genre']
        db.create_unique(u'news_newssourcegenre', ['source_id', 'genre_id'])

        # Adding model 'Genre'
        db.create_table(u'news_genre', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=100)),
        ))
        db.send_create_signal(u'news', ['Genre'])

        # Adding model 'GenreKeyword'
        db.create_table(u'news_genrekeyword', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('genre', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.Genre'])),
            ('keyword', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.Keyword'])),
        ))
        db.send_create_signal(u'news', ['GenreKeyword'])

        # Adding unique constraint on 'GenreKeyword', fields ['genre', 'keyword']
        db.create_unique(u'news_genrekeyword', ['genre_id', 'keyword_id'])

        # Adding model 'Keyword'
        db.create_table(u'news_keyword', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
        ))
        db.send_create_signal(u'news', ['Keyword'])

        # Adding model 'KeywordRelative'
        db.create_table(u'news_keywordrelative', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('basis', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.Keyword'])),
            ('other', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm['news.Keyword'])),
            ('closeness', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'news', ['KeywordRelative'])

        # Adding unique constraint on 'KeywordRelative', fields ['basis', 'other']
        db.create_unique(u'news_keywordrelative', ['basis_id', 'other_id'])

    def backwards(self, orm):
        # Removing unique constraint on 'KeywordRelative', fields ['basis', 'other']
        db.delete_unique(u'news_keywordrelative', ['basis_id', 'other_id'])

        # Removing unique constraint on 'GenreKeyword', fields ['genre', 'keyword']
        db.delete_unique(u'news_genrekeyword', ['genre_id', 'keyword_id'])

        # Removing unique constraint on 'NewsSourceGenre', fields ['source', 'genre']
        db.delete_unique(u'news_newssourcegenre', ['source_id', 'genre_id'])

        # Removing unique constraint on 'NewsArticleKeyword', fields ['article', 'keyword']
        db.delete_unique(u'news_newsarticlekeyword', ['article_id', 'keyword_id'])

        # Removing unique constraint on 'NewsArticleGenre', fields ['article', 'genre']
        db.delete_unique(u'news_newsarticlegenre', ['article_id', 'genre_id'])

        # Deleting model 'NewsArticle'
        db.delete_table(u'news_newsarticle')

        # Deleting model 'NewsArticleGenre'
        db.delete_table(u'news_newsarticlegenre')

        # Deleting model 'NewsArticleKeyword'
        db.delete_table(u'news_newsarticlekeyword')

        # Deleting model 'NewsSource'
        db.delete_table(u'news_newssource')

        # Deleting model 'NewsSourceGenre'
        db.delete_table(u'news_newssourcegenre')

        # Deleting model 'Genre'
        db.delete_table(u'news_genre')

        # Deleting model 'GenreKeyword'
        db.delete_table(u'news_genrekeyword')

        # Deleting model 'Keyword'
        db.delete_table(u'news_keyword')

        # Deleting model 'KeywordRelative'
        db.delete_table(u'news_keywordrelative')

    models = {
        u'news.genre': {
            'Meta': {'object_name': 'Genre'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'genres'", 'to': u"orm['news.Keyword']", 'through': u"orm['news.GenreKeyword']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'news.genrekeyword': {
            'Meta': {'unique_together': "((u'genre', u'keyword'),)", 'object_name': 'GenreKeyword'},
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.Genre']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keyword': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.Keyword']"})
        },
        u'news.keyword': {
            'Meta': {'object_name': 'Keyword'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'relatives': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['news.Keyword']", 'null': 'True', 'through': u"orm['news.KeywordRelative']", 'blank': 'True'})
        },
        u'news.keywordrelative': {
            'Meta': {'unique_together': "((u'basis', u'other'),)", 'object_name': 'KeywordRelative'},
            'basis': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.Keyword']"}),
            'closeness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'other': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.Keyword']"})
        },
        u'news.newsarticle': {
            'Meta': {'object_name': 'NewsArticle'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'news'", 'to': u"orm['news.Genre']", 'through': u"orm['news.NewsArticleGenre']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': (u'main.db.fields.AutoResizeImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'keywords': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'news'", 'to': u"orm['news.Keyword']", 'through': u"orm['news.NewsArticleKeyword']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'published': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'news.newsarticlegenre': {
            'Meta': {'unique_together': "((u'article', u'genre'),)", 'object_name': 'NewsArticleGenre'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.NewsArticle']"}),
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.Genre']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'news.newsarticlekeyword': {
            'Meta': {'unique_together': "((u'article', u'keyword'),)", 'object_name': 'NewsArticleKeyword'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.NewsArticle']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keyword': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.Keyword']"})
        },
        u'news.newssource': {
            'Meta': {'object_name': 'NewsSource'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'news_sources'", 'to': u"orm['news.Genre']", 'through': u"orm['news.NewsSourceGenre']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastfetch': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'news.newssourcegenre': {
            'Meta': {'unique_together': "((u'source', u'genre'),)", 'object_name': 'NewsSourceGenre'},
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.Genre']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['news.NewsSource']"})
        }
    }

    complete_apps = ['news']
