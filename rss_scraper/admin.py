# coding: utf-8
from __future__ import (
    absolute_import,
    unicode_literals,
)

from django.contrib import admin

from news.models import (
    Genre,
    GenreKeyword,
    Keyword,
    NewsArticle,
    NewsArticleGenre,
    NewsArticleKeyword,
    NewsSource,
    NewsSourceGenre,
)


class GenreAdmin(admin.ModelAdmin):
    model = Genre
    list_display = ('name',)


class GenreKeywordAdmin(admin.ModelAdmin):
    model = GenreKeyword
    list_display = ('genre', 'keyword')


class KeywordAdmin(admin.ModelAdmin):
    model = Keyword
    list_display = ('name',)


class NewsArticleAdmin(admin.ModelAdmin):
    model = NewsArticle
    list_display = ('title', 'url')


class NewsArticleGenreAdmin(admin.ModelAdmin):
    model = NewsArticleGenre
    list_display = ('article', 'genre')


class NewsArticleKeywordAdmin(admin.ModelAdmin):
    model = NewsArticleKeyword
    list_display = ('article', 'keyword')


class NewsSourceAdmin(admin.ModelAdmin):
    model = NewsSource
    list_display = ('name', 'url')


class NewsSourceGenreAdmin(admin.ModelAdmin):
    model = NewsSourceGenre
    list_display = ('source', 'genre')


for Model, ModelAdmin in (
    (Genre, GenreAdmin),
    (GenreKeyword, GenreKeywordAdmin),
    (Keyword, KeywordAdmin),
    (NewsArticle, NewsArticleAdmin),
    (NewsArticleGenre, NewsArticleGenreAdmin),
    (NewsArticleKeyword, NewsArticleKeywordAdmin),
    (NewsSource, NewsSourceAdmin),
    (NewsSourceGenre, NewsSourceGenreAdmin),
):
    admin.site.register(Model, ModelAdmin)
