# coding: utf-8
from __future__ import (
    absolute_import,
    unicode_literals,
)

import hashlib
import os
from xml.etree import ElementTree

from django.db import models
from django.utils.translation import ugettext as _

from model_utils.models import TimeStampedModel

from main.db.fields import AutoResizeImageField


optional = {
    'blank': True,
    'null': True,
}


def get_article_image_dir(article, filename):
    # note that `article` is, most of the times, not saved in the database yet
    cipher = hashlib.sha256()
    cipher.update(article.url)
    cipher.update(article.title)

    folder = cipher.hexdigest()[:15]

    return os.path.join('news-article', folder, filename)


class NewsArticle(TimeStampedModel):

    """
    """
    url = models.URLField()
    title = models.CharField(max_length=1024)
    description = models.TextField()
    image = AutoResizeImageField(upload_to=get_article_image_dir,
                                 max_height=256,
                                 max_width=256,
                                 **optional)
    published = models.DateTimeField()
    genres = models.ManyToManyField('Genre',
                                    related_name='news',
                                    through='NewsArticleGenre',
                                    **optional)
    keywords = models.ManyToManyField('Keyword',
                                      related_name='news',
                                      through='NewsArticleKeyword',
                                      **optional)

    class Meta:
        verbose_name_plural = _('news articles')
        verbose_name = _('news article')

    def __unicode__(self):
        return self.title

    def to_rss_document(self):
        item = ElementTree.Element('item')

        subel = ElementTree.SubElement(item, 'guid', isPermalink='true')
        subel.text = self.url
        subel = ElementTree.SubElement(item, 'link')
        subel.text = self.url
        subel = ElementTree.SubElement(item, 'title')
        subel.text = self.title
        subel = ElementTree.SubElement(item, 'description')
        subel.text = self.description
        subel = ElementTree.SubElement(item, 'pubDate')
        subel.text = self.published.strftime('%a, %d %b %Y %H:%M:%S %z')

        return item


class NewsArticleGenre(models.Model):
    article = models.ForeignKey('NewsArticle', related_name='+')
    genre = models.ForeignKey('Genre', related_name='+')

    class Meta:
        verbose_name_plural = _('news article genres')
        verbose_name = _('news article genre')
        unique_together = ('article', 'genre')

    def __unicode__(self):
        return '%s: %s' % (self.article, self.genre)


class NewsArticleKeyword(models.Model):
    article = models.ForeignKey('NewsArticle', related_name='+')
    keyword = models.ForeignKey('Keyword', related_name='+')

    class Meta:
        verbose_name_plural = _('news article keywords')
        verbose_name = _('news article keyword')
        unique_together = ('article', 'keyword')

    def __unicode__(self):
        return '%s: %s' % (self.article, self.keyword)


class NewsSource(TimeStampedModel):

    """
    represents a website RSS URL acting as a source for news to be scraped on.
    """
    TYPE_GENERAL = 1
    TYPE_SPECIAL = 2
    TYPE_CHOICES = (
        (TYPE_GENERAL, _('General')),
        (TYPE_SPECIAL, _('Specialized')),
    )

    url = models.URLField()
    name = models.CharField(max_length=100)
    type = models.SmallIntegerField(default=TYPE_GENERAL, choices=TYPE_CHOICES)
    lastfetch = models.DateTimeField(**optional)
    genres = models.ManyToManyField('Genre',
                                    related_name='news_sources',
                                    through='NewsSourceGenre',
                                    **optional)

    class Meta:
        verbose_name_plural = _('news sources')
        verbose_name = _('news source')

    def __unicode__(self):
        return self.name


class NewsSourceGenre(models.Model):
    source = models.ForeignKey('NewsSource', related_name='+')
    genre = models.ForeignKey('Genre', related_name='+')

    class Meta:
        verbose_name_plural = _('news source genres')
        verbose_name = _('news source genre')
        unique_together = ('source', 'genre')

    def __unicode__(self):
        return '%s: %s' % (self.source, self.genre)


class Genre(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    keywords = models.ManyToManyField('Keyword',
                                      related_name='genres',
                                      through='GenreKeyword',
                                      **optional)

    class Meta:
        verbose_name_plural = _('genres')
        verbose_name = _('genre')

    def __unicode__(self):
        return self.name


class GenreKeyword(models.Model):
    genre = models.ForeignKey('Genre', related_name='+')
    keyword = models.ForeignKey('Keyword', related_name='+')

    class Meta:
        verbose_name_plural = _('genre keywords')
        verbose_name = _('genre keyword')
        unique_together = ('genre', 'keyword')

    def __unicode__(self):
        return '%s:%s' % (self.genre, self.keyword)


class Keyword(models.Model):
    name = models.CharField(max_length=100, unique=True)
    relatives = models.ManyToManyField('self',
                                       through='KeywordRelative',
                                       symmetrical=False,
                                       **optional)

    class Meta:
        verbose_name_plural = _('keywords')
        verbose_name = _('keyword')

    def __unicode__(self):
        return self.name


class KeywordRelative(models.Model):
    basis = models.ForeignKey('Keyword', related_name='+')  # cars
    other = models.ForeignKey('Keyword', related_name='+')  # sports
    closeness = models.IntegerField(default=0)                 # the number of times "sports" appeared with "cars"

    class Meta:
        verbose_name_plural = _('keyword relatives')
        verbose_name = _('keyword relative')
        unique_together = ('basis', 'other')

    def __unicode__(self):
        return '%s --> %s' % (self.basis, self.other)
