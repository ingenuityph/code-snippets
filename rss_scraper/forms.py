# coding: utf-8
from __future__ import (
    absolute_import,
    unicode_literals,
)

from django.forms import ModelForm

from news.models import (
    NewsArticle,
)


class NewsArticleForm(ModelForm):

    class Meta:
        model = NewsArticle
