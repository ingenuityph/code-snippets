# coding: utf-8
from __future__ import (
    absolute_import,
    unicode_literals,
)

from django.conf.urls import (
    include,
    patterns,
    url,
)


urlpatterns = patterns('news.views.web',
                       url(r'^(?P<genre>\w+)\.rss$', 'genre_listing', name='genre-listing'),
                       )
