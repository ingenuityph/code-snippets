# coding: utf-8
from __future__ import (
    absolute_import,
    unicode_literals,
)

import datetime
import re
import time
import urllib
import urllib2
from urllib2 import (
    HTTPError,
    Request,
    URLError,
)

import feedparser
from bs4 import BeautifulSoup
from django.utils import timezone

from news.forms import (
    NewsArticleForm,
)
from news.models import (
    Genre,
    Keyword,
    NewsArticle,
    NewsArticleGenre,
    NewsArticleKeyword,
)


class ArticleRSSParser(object):

    """
    Handles fetching of data then saving the article data to the database
    """

    def __init__(self):
        self.reset()

    def reset(self):
        self.keyword_parser = None
        self.source = None
        self.source_genres = None

    def set_source(self, source):
        self.source = source
        self.source_genres = self.source.genres.all()

    def get_keyword_parser(self):
        if not self.source:
            self.keyword_parser = None
        elif not self.keyword_parser:
            parser = ArticleKeywordParser()
            parser.set_genres(self.source_genres)

            self.keyword_parser = parser

        return self.keyword_parser

    def get_article_keywords(self, article):
        parser = self.get_keyword_parser()
        parser.set_article(article)

        return parser.parse()

    def get_article_genres(self, article):
        return self.source_genres

    def save_records(self, summaries):
        for summary in summaries:
            article = summary.get('article')
            article.save()

            NewsArticleKeyword.objects.bulk_create([
                NewsArticleKeyword(**{
                    'article': article,
                    'keyword': keyword,
                }) for keyword in summary.get('keywords')
            ])

            NewsArticleGenre.objects.bulk_create([
                NewsArticleGenre(**{
                    'article': article,
                    'genre': genre,
                }) for genre in summary.get('genres')
            ])

    def parse(self):
        summaries = []
        rss = feedparser.parse(self.source.url)

        for entry in rss.entries:
            data = {
                'url': entry.get('link'),
                'title': entry.get('title'),
                'description': entry.get('description'),
                'published': datetime.datetime.fromtimestamp(time.mktime(entry.get('published_parsed')),
                                                             tz=timezone.utc),
                'genres': [genre.id for genre in self.source_genres],
            }
            form = NewsArticleForm(data)

            if not form.is_valid():
                continue

            article = form.save(commit=False)
            genres = self.get_article_genres(article)
            registry = self.get_article_keywords(article)
            keywords = registry.keys()

            if not keywords:
                continue

            summaries.append({
                'article': article,
                'genres': genres,
                'keywords': keywords,
                'keywords_registry': registry,
            })

        self.save_records(summaries)

        return summaries


class ArticleKeywordParser(object):

    def set_genres(self, genres):
        self.genres = genres
        self.mapping = None
        self.matcher = None

        self.init_keywords_mapping()
        self.init_keywords_matcher()

    def set_article(self, article):
        self.article = article

    def init_keywords_mapping(self):
        if self.mapping or not self.genres:
            return

        keywords = []
        for genre in self.genres:
            keywords += list(genre.keywords.all())
        # remove duplicates
        keywords = list(set(keywords))

        self.mapping = {
            keyword.name: keyword
            for keyword in keywords
        }

    def init_keywords_matcher(self):
        if self.matcher:
            return

        if not self.mapping:
            keywords = []

        keywords = self.mapping.values()
        pattern = '|'.join([keyword.name for keyword in keywords])
        self.matcher = re.compile(pattern)

    def combine_registry(self, destination, *sources):
        destination = destination or {}

        for source in sources:
            for keyword, count in source.iteritems():
                count_dst = destination.setdefault(keyword, 0)
                count_dst += count

                destination.update({keyword: count_dst})

        return destination

    def register_keyword(self, keyword, founddict):
        count = founddict.setdefault(keyword, 0)
        count += 1

        founddict.update({keyword: count})

    def parse_text(self, text):
        matches = self.matcher.finditer(text)
        keywords = {}

        for match in matches:
            matchstr = text[match.start():match.end()]

            self.register_keyword(self.mapping.get(matchstr), keywords)

        return keywords

    def parse_content(self):
        keywords = {}
        request = Request(self.article.url)

        try:
            response = urllib2.urlopen(request)
            html = response.read()
        except (HTTPError, URLError) as error:
            return keywords

        soup = BeautifulSoup(html)

        for excludes in ('script', 'iframe', 'noscript'):
            for excluded in soup.select(excludes):
                excluded.decompose()

        htmltag = soup.select('head meta[name="keywords"]')
        if htmltag:
            for metakeyword in htmltag[0].get('content').split(','):
                if metakeyword not in self.mapping:
                    continue

                self.register_keyword(self.mapping.get(metakeyword), keywords)

        htmltag = soup.select('head meta[name="description"]')
        if htmltag:
            keywords_tmp = self.parse_text(htmltag[0].get('content'))
            keywords = self.combine_registry(keywords, keywords_tmp)

        keywords_tmp = self.parse_text(unicode(soup.body))
        keywords = self.combine_registry(keywords, keywords_tmp)

        return keywords

    def parse(self):
        keywords = self.combine_registry(
            {},
            self.parse_text(self.article.title),
            self.parse_text(self.article.description),
            self.parse_content()
        )

        return keywords
