# coding: utf-8
from __future__ import (
    absolute_import,
    unicode_literals,
)

import datetime

from django.test import TestCase
from django.utils import timezone

from news.models import (
    Genre,
    GenreKeyword,
    Keyword,
    NewsArticle,
    NewsArticleGenre,
    NewsArticleKeyword,
    NewsSource,
    NewsSourceGenre,
)
from news.parsers import (
    ArticleRSSParser,
    ArticleKeywordParser,
)


class ArticleExtractorTestCase(TestCase):

    def setUp(self):
        sources = []
        sources_data = (
            {
                'url': 'http://response.jp/rss20/index.rdf',
                'type': NewsSource.TYPE_SPECIAL,
                'name': 'Response'
            },
            # {
            #     'url' : 'http://car.watch.impress.co.jp/docs/car.rdf',
            #     'type': NewsSource.TYPE_GENERAL,
            #     'name': 'Car Watch'
            # },
            # {
            #     'url' : 'http://www.rbbtoday.com/rss/index.rdf',
            #     'type': NewsSource.TYPE_GENERAL,
            #     'name': 'RBB Today'
            # },
            # {
            #     'url' : 'http://feed.rssad.jp/rss/autoblog/rss ',
            #     'type': NewsSource.TYPE_GENERAL,
            #     'name': 'RSS Ad'
            # },
        )
        for source_data in sources_data:
            source = NewsSource(**source_data)
            source.save()
            sources.append(source)

        genres = []
        genres_data = (
            {'name': 'Cars'},
            {'name': 'Games'},
        )
        for genre_data in genres_data:
            genre = Genre(**genre_data)
            genre.save()
            genres.append(genre)

        keywords = []
        keywords_data = (
            {'name': 'ル'},
            {'name': '車'},
            {'name': '自動車業界'},
            {'name': '企業動向'},
        )
        for keyword_data in keywords_data:
            keyword = Keyword(**keyword_data)
            keyword.save()
            keywords.append(keyword)

        for source in sources:
            for genre in genres:
                NewsSourceGenre(**{
                    'source': source,
                    'genre': genre
                }).save()

                for keyword in keywords:
                    GenreKeyword(**{
                        'genre': genre,
                        'keyword': keyword,
                    }).save()

    def tearDown(self):
        NewsArticleGenre.objects.all().delete()
        NewsSourceGenre.objects.all().delete()
        NewsArticle.objects.all().delete()
        NewsSource.objects.all().delete()
        Genre.objects.all().delete()
        Keyword.objects.all().delete()

    def testRSSParsing(self):
        source = NewsSource.objects.get(url='http://response.jp/rss20/index.rdf')
        parser = ArticleRSSParser()
        parser.set_source(source)

        summaries = parser.parse()

        self.assertTrue(NewsArticle.objects.count() > 0)
        self.assertTrue(NewsArticleGenre.objects.count() > 0)
        self.assertTrue(NewsArticleKeyword.objects.count() > 0)

        self.assertTrue(len(summaries) > 0)

        source_genres = [genre.id for genre in source.genres.all()]
        for article in NewsArticle.objects.all():
            self.assertTrue(article.genres.count() > 0)
            self.assertTrue(article.keywords.count() > 0)
            self.assertIsNotNone(article.description)


class KeywordExtractorTestCase(TestCase):

    def setUp(self):
        article = NewsArticle(**{
            'url': 'http://response.jp/article/2014/07/09/227222.html',
            'title': 'VW の中国工場、独メルケル首相が訪問…最先端の生産現場を見学',
            'description': '欧州の自動車最大手、フォルクスワーゲングループは7月6日、ドイツのメルケル首相が、同社の中国四川省の成都工場を訪れたと発表した。',
            'published': datetime.datetime(2014, 7, 9, 2, 0, 3, tzinfo=timezone.utc)
        })
        article.save()

        genres = []
        genres_data = (
            {'name': 'Cars'},
            {'name': 'Games'},
        )
        for genre_data in genres_data:
            genre = Genre(**genre_data)
            genre.save()
            genres.append(genre)

        keywords = []
        keywords_data = (
            {'name': 'ル'},
            {'name': '車'},
            {'name': '自動車業界'},
            {'name': '企業動向'},
        )
        for keyword_data in keywords_data:
            keyword = Keyword(**keyword_data)
            keyword.save()
            keywords.append(keyword)

        genre = Genre.objects.get(name='Cars')
        for keyword in keywords:
            GenreKeyword(**{
                'genre': genre,
                'keyword': keyword
            }).save()

        for genre in genres:
            NewsArticleGenre(**{
                'article': article,
                'genre': genre,
            }).save()

    def tearDown(self):
        GenreKeyword.objects.all().delete()
        NewsArticleKeyword.objects.all().delete()

        NewsArticle.objects.all().delete()
        Genre.objects.all().delete()
        Keyword.objects.all().delete()

    def testRegistryMerging(self):
        genres = Genre.objects.all()
        parser = ArticleKeywordParser()
        parser.set_genres(genres)

        reg1 = {
            '自動車業界': 2,
            '車': 1,
            'ル': 1,
        }
        reg2 = {
            '自動車業界': 2,
            '企業動向': 3,
        }
        reg3 = {
            '自動車業界': 1,
            'ル': 4,
        }
        reg4 = {
            '車': 3,
            '企業動向': 2,
        }
        reg5 = {
            '車': 1,
        }
        merged = parser.combine_registry(reg1, reg2, reg3, reg4, reg5)

        self.assertEqual(id(merged), id(reg1))
        for key, value in merged.iteritems():
            self.assertEqual(value, 5)

    def testKeywordMapping(self):
        genre = Genre.objects.get(name='Cars')
        parser = ArticleKeywordParser()
        parser.set_genres([genre])

        mapping = parser.mapping
        keywords = genre.keywords.all()

        self.assertEqual(len(mapping.keys()), keywords.count())

        for key, keyword in mapping.iteritems():
            self.assertEqual(key, keyword.name)

    def testKeywordParsing(self):
        genres = Genre.objects.filter(name='Cars')
        parser = ArticleKeywordParser()
        parser.set_genres(genres)

        keyword1 = Keyword.objects.get(name='ル')
        keyword2 = Keyword.objects.get(name='車')
        keywords = parser.parse_text('VW の中国工場、独メルケル首相が訪問…最先端の生産現場を見学')

        self.assertTrue(keyword1 in keywords)
        self.assertTrue(keyword2 not in keywords)
        self.assertEqual(len(keywords.keys()), 1)
        self.assertEqual(keywords.get(keyword1), 2)

        keyword1 = Keyword.objects.get(name='ル')
        keyword2 = Keyword.objects.get(name='車')
        keyword3 = Keyword.objects.get(name='自動車業界')
        keywords = parser.parse_text('欧州の自動車最大手、フォルクスワーゲングループは7月6日、ドイツのメルケル首相が、同社の中国四川省の成都工場を訪れたと発表した。')

        self.assertTrue(keyword1 in keywords and keyword2 in keywords)
        self.assertTrue(keyword3 not in keywords)
        self.assertEqual(2, len(keywords.keys()))
        self.assertEqual(4, keywords.get(keyword1))
        self.assertEqual(1, keywords.get(keyword2))

    def testArticleScraping(self):
        article = NewsArticle.objects.get(url='http://response.jp/article/2014/07/09/227222.html')
        genres = article.genres.all()
        parser = ArticleKeywordParser()
        parser.set_genres(genres)
        parser.set_article(article)

        keywords = parser.parse()
        keyword1 = Keyword.objects.get(name='ル')
        keyword2 = Keyword.objects.get(name='車')
        keyword3 = Keyword.objects.get(name='企業動向')
        keyword4 = Keyword.objects.get(name='自動車業界')

        for keyword, count in keywords.iteritems():
            self.assertTrue(isinstance(keyword, Keyword))

        self.assertEqual(3, len(keywords.keys()))
        self.assertTrue(keyword4 not in keywords)
