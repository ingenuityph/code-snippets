# coding: utf-8
from __future__ import (
    absolute_import,
    unicode_literals,
)

from django.core import urlresolvers
from django.test import TestCase

from news.views.api import (
    GenreIndexView,
)


class GenreIndexViewTestCase(TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testUrlResolution(self):
        urlresolvers.reverse('api:news:genre-index', kwargs={'genre': 'Sample'})
        urlresolvers.reverse('api:news:genre-index', kwargs={'genre': 'Sample', 'keyword': 'car'})
