# coding: utf-8
from __future__ import (
    absolute_import,
    unicode_literals,
)

import httplib
from xml.etree import ElementTree

from django.contrib.sites.models import Site
from django.http.response import HttpResponse
from django.views.generic import View
from django.utils.translation import ugettext as _

import main.utils
from main.views.mixins import WebViewMixin
from news.models import (
    Genre,
    Keyword,
    NewsArticle,
    NewsSource,
)


class GenreListingView(WebViewMixin, View):

    def get_genre(self, *args, **kwargs):
        slug = kwargs.get('genre')
        genre = main.utils.get_object_gracefully(Genre, slug=slug)

        return genre

    def get(self, request, *args, **kwargs):
        genre = self.get_genre(*args, **kwargs)
        site = Site.objects.get_current()
        root = ElementTree.Element('rss', version='2.0')
        channel = ElementTree.SubElement(root, 'channel')

        if genre:
            status = httplib.OK
            site = Site.objects.get_current()
            articles = genre.news.all()

            subel = ElementTree.SubElement(channel, 'title')
            subel.text = site.name
            subel = ElementTree.SubElement(channel, 'link')
            subel.text = 'http://%s' % site.domain
            subel = ElementTree.SubElement(channel, 'description')
            subel.text = _('What am I gonna put here haha')
            subel = ElementTree.SubElement(channel, 'language')
            subel.text = 'ja'

            channel.extend([article.to_rss_document() for article in articles])
        else:
            status = httplib.NOT_FOUND

        return HttpResponse(ElementTree.tostring(root, encoding='utf-8'), **{
            'status': status,
            'content_type': 'text/xml',
        })
genre_listing = GenreListingView.as_view()
