# coding: utf-8
from __future__ import (
    absolute_import,
    unicode_literals,
)

from django.views.generic import View

from main.views.mixins import APIViewMixin


class GenreIndexView(APIViewMixin, View):
    pass
genre_index = GenreIndexView.as_view()
